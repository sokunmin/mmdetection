# TODO in priority order
1. map ckpt from official EmbedMask to mmdet by print(model)
1. CHECK `init_weights()` in `SimplePose` 
1. find out why speed is slow. 
1. compare w/ `RDSNet`
1. `ASFF`
1. `KL-Loss`
1. `InstaBoost`
1. `DIOU-NMS`
1. use `H-swish` in fcos head
1. replace torch API with cuda ext in EmbedMask.
1. change EmbedMask target generation to CPU
1. iResGroup + GeneralizedAttention
1. Remove `extra_check`
1. EvoNorm - https://mp.weixin.qq.com/s/tk869ojbqXv25jMWL00nuw
1

# Experiments
* [ ] **Experiment**: EmbedMask 
  * [ ] train w/ my augmentation
  * [ ] fine-tune `loss_weight` in `mask_head`
  * [ ] refactor `NMS` code snippets in EmbedMask head
  * [ ] change to 
  * [ ] add `reduction='sum'` when `AutoLoss` is added in configs
  * [ ] test `LR-ASPP`

## Backbone
* [ ] [FBNetV3: Joint Architecture-Recipe Search using
Neural Acquisition Function]()

## FPN
* [ ] [`Panoptic FPN`](https://arxiv.org/abs/1901.02446)
  * [ ] Integrate Panoptic FPN
* [ ] Try `DCN` in `RepPoints`
* [ ] Test on `InstaBoost`   
* [ ] IMP: Instance Mask Projection for High Accuracy Semantic Segmentation of Things
* [ ] AdaptIS: Adaptive Instance Selection Network
* [ ] [ASFF](https://github.com/ruinmessi/ASFF)

## Detection
* [ ] Add `EfficientDet`
* [ ] [AlignDet - Revisiting Feature Alignment for One-stage Object Detection](https://arxiv.org/pdf/1908.01570.pdf)
* [ ] [FCOS + ATSS](https://github.com/sfzhang15/ATSS)
* [ ] [DeepLab ASPP v1,v2,v3,v3+](https://zhuanlan.zhihu.com/p/68531147)

## Segmentation
* `EmbedMask`
  * [ ] 1x1 conv first + SE module, then add `MobileNetv3` segm head to FCOS head__
  * [ ] replace `upsampling` with `carafe`
  * [ ] add `RepPoints` head to FCOS head
  * [ ] add `DCNv2`, see `YOLACT++`
  * [ ] add `DIoU-NMS`
  * [ ] optimize `mask_prob` computing
    * [ ] refactor code pattern
    * [ ] add [cuda support](https://github.com/yinghdb/EmbedMask/commit/093348eface286f2fbe04cd6a08d79ea1f7c8ac3)
* [ ] [BlendMask](https://arxiv.org/abs/2001.00309)    
* [ ] [RDSNet](https://arxiv.org/pdf/1912.05070.pdf) on mmdet
  * [ ] https://github.com/wangsr126/RDSNet
* [ ] [PointRend: Image Segmentation as Rendering](https://arxiv.org/pdf/1912.08193.pdf)
* [ ] [SegLoss collection](https://github.com/JunMa11/SegLoss)
* [ ] [Region Mutual Information Loss for Semantic Segmentation](https://arxiv.org/pdf/1910.12037.pdf)
* [ ] [SOLO: Segmenting Objects by Locations](https://github.com/WXinlong/SOLO)
  * [ ] merge to mmdetection

## Pose Estimation
* Add pose head to `SingleStageDetector`
  * [ ] [Detectron2](https://github.com/facebookresearch/detectron2)
  * [ ] Add `OHEM` for keypoints as a new sampler
  * [ ] Add `OKS-NMS`
  * [ ] Adopt `PoseFix`
* [ ] [Pose2Seg](https://arxiv.org/pdf/1803.10683.pdf)
* [x] [PersonLab]()
* [x] [PifPaf](https://arxiv.org/pdf/1903.06593.pdf)
  * [x] [source code](https://github.com/vita-epfl/openpifpaf)
* [x] [Higher HRNet](https://arxiv.org/abs/1908.10357)
* [x] [DirectPose](https://arxiv.org/pdf/1911.07451.pdf)
* [ ] [PoseFix](https://github.com/mks0601/PoseFix_RELEASE)
* [x] [SimplePose](https://arxiv.org/abs/1911.10529)
  * [ ] change BN to GN
  * [ ] add CARAFE 
  * [ ] Transfer learning: train w/ ImageNet
  * [ ] iResGroup
* [ ] [Animal Pose Dataset](https://arxiv.org/abs/1908.05806)
* [ ] [**MSPN - Rethinking on Multi-Stage Networks for Human Pose Estimation**]()
* [ ] [**Simple and Lightweight Human Pose Estimation]**()

## Loss
* [ ] Add [`KL-Loss`](https://github.com/yihui-he/KL-Loss)
* [ ] Add [DTP: Dynamic Task Prioritization for Multitask Learning](http://openaccess.thecvf.com/content_ECCV_2018/papers/Michelle_Guo_Focus_on_the_ECCV_2018_paper.pdf)
* [ ] Add [GradNorm: Gradient Normalization for Adaptive Loss Balancing in Deep Multitask Networks](https://arxiv.org/abs/1711.02257)
* [ ] Add [Class-balanced Loss](https://github.com/vandit15/Class-balanced-loss-pytorch)

## Metric Learning
* [ ] [N-pairs Loss](http://www.nec-labs.com/uploads/images/Department-Images/MediaAnalytics/papers/nips16_npairmetriclearning.pdf)
  * This improved TripletLoss
* [ ] [Multi-Similarity Loss with General Pair Weighting for Deep Metric Learning](https://arxiv.org/abs/1904.06627)
* [ ] [SoftTriple](t.ly/d1Vz1)
  * This works better than `N-pairs Loss` and `SoftMax_norm`
* [ ] [Deep Metric Learning with Tuplet Margin Loss](t.ly/DrjAD)
  * This also works better than `N-pairs Loss`
* [ ] [DMML](bit.ly/2G6vro1)
  * This also works better than `N-pairs Loss`
 
## Optimization
* [ ] `DIoU-NMS`

## Domain Adaptation
* [ ] [IBN-NET: Instance-Batch Normalization Network](https://github.com/XingangPan/IBN-Net)

## Action / Video
* [ ] [DMM-Net](https://arxiv.org/abs/1909.12471)

## Tracking
* [ ] [SiamCAR](https://github.com/ohhhyeahhh/SiamCAR)
* [ ] [**Efficient Online Multi-Person 2D Pose Tracking with Recurrent Spatio-Temporal Affinity Fields**]()
