# Created by Chun-Ming Su, 02/04/2020
from mmdet.apis import init_detector, inference_detector, show_result_pyplot, show_result
import torch
import os
import glob
import argparse
import matplotlib.pyplot as plt
from mmcv.image import imread, imwrite

from mmdet.core.evaluation.class_names import get_ckpt_converter


def parse_args():
    parser = argparse.ArgumentParser(description='MMDetection image demo')
    parser.add_argument('config', help='test config file path')
    parser.add_argument('checkpoint', help='checkpoint file')
    parser.add_argument(
        '--ckpt-module', choices=['mmcv', 'fcos', 'embedmask', 'simplepose'],
        default='mmcv', help='name of converter')
    parser.add_argument(
        '--pose', choices=['none', 'coco', 'cmu'],
        default='none', help='type of pose estimation')
    parser.add_argument('--device', type=int, default=0, help='CUDA device id')
    parser.add_argument(
        '--image-dir', type=str, default='data/demo', help='image directory')
    parser.add_argument(
        '--output-dir', type=str, default='data/output_images', help='image directory')
    parser.add_argument(
        '--score-thr', type=float, default=0.5, help='bbox score threshold')
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    ckpt_module = get_ckpt_converter(args.ckpt_module)
    # build the model from a config file and a checkpoint file
    model = init_detector(
        args.config, args.checkpoint, device=torch.device('cuda', args.device),
        ckpt_module=ckpt_module, pose=args.pose)

    # test images in the path `args.image_dir`
    images = []
    for ext in ('*.jpg', '*.jpeg', '*.png'):
        images.extend(glob.glob(os.path.join(args.image_dir, ext)))

    for i, img in enumerate(images):
        result = inference_detector(model, img)

        # show the results
        classes = model.CLASSES
        if isinstance(classes, str):
            classes = [classes]
        skeletons = (model.KEYPOINTS, model.BODYPARTS) if args.pose != 'none' else None
        img = show_result_pyplot(img, result, classes, score_thr=args.score_thr, pose_pairs=skeletons)
        out_file = os.path.join(args.output_dir, str(i) + '.jpg')
        imwrite(img, out_file)
        plt.show()


if __name__ == '__main__':
    main()
