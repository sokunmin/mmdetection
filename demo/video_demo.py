import cv2
import mmcv
import argparse
import torch
import matplotlib.pyplot as plt
from mmdet.apis import init_detector, inference_detector, show_result_pyplot, show_result
from mmdet.core.evaluation.class_names import get_ckpt_converter


def parse_args():
    parser = argparse.ArgumentParser(description='MMDetection image demo')
    parser.add_argument('config', help='test config file path')
    parser.add_argument('checkpoint', help='checkpoint file')
    parser.add_argument(
        '--ckpt-module', choices=['mmcv', 'fcos', 'embedmask', 'simplepose'],
        default='mmcv', help='name of converter')
    parser.add_argument(
        '--pose', choices=['none', 'coco', 'cmu'],
        default='none', help='type of pose estimation')
    parser.add_argument('--device', type=int, default=0, help='CUDA device id')
    parser.add_argument(
        '--video', type=str, default='data/demo_videos/demo.mp4', help='video directory')
    parser.add_argument(
        '--output-dir', type=str, default='data/output_images', help='video directory')
    parser.add_argument(
        '--score-thr', type=float, default=0.5, help='bbox score threshold')
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    ckpt_module = get_ckpt_converter(args.ckpt_module)

    # build the model from a config file and a checkpoint file
    model = init_detector(
        args.config, args.checkpoint, device=torch.device('cuda', args.device),
        ckpt_module=ckpt_module, pose=args.pose)

    # test a video and show the results
    video = mmcv.VideoReader(args.video)
    for frame in video:
        result = inference_detector(model, frame)
        skeletons = (model.KEYPOINTS, model.BODYPARTS) if args.pose != 'none' else None

        frame_out = show_result(frame, result, model.CLASSES, wait_time=1, pose_pairs=skeletons)
        if frame_out is not None:
            plt.imshow(frame_out)
        else:
            plt.imshow(frame)
        plt.show()


if __name__ == '__main__':
    main()
