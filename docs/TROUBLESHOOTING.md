# `NaN` in training loss
* lower learning rate
* set gradient clip in config
* weight the loss
    * for example, set 1.0 to 0.5 
    * set `loss_weight=0.5` in config
* clamp the loss (sometimes loss may be large)
    * call `loss.clamp(max=1.0)` is loss function

# Out Of Memory (OOM)
* When training is interrupted accidentally, enter `nvidia-smi` to check where deadlocks occur in GPU
  * if yes, kill processes, and restart/resume training
  * to kill processes, enter `kill -9 #proc_id`, where `#proc_id` means process id shown in `nvidia-smi`.
* check which line of code corresponding to OOM. Trace back and optimize your code.
* check configs 
    * like `imgs_per_gpu` and `workers_per_gpu`. Make the number smaller.
    * `num_classes` should be matched according to your tasks. 
    
# Others
## Error 1
```
RuntimeError: one of the variables needed for gradient computation has been modified by an inplace operation: 
[torch.cuda.FloatTensor [4, 3, 513, 513]], 
which is output 0 of ReluBackward1, is at version 3; 
expected version 1 instead. 
Hint: enable anomaly detection to find the operation that failed to compute its gradient, 
with torch.autograd.set_detect_anomaly(True).
```
## Solution:
1. 由於新版本的pytorch把Varible和Tensor融合為一個Tensor，inplace操作，之前對Varible能用，但現在對Tensor，就會出錯了。
    ```
    x = x + 1 # not inplace
    x += 1 # inplace
    ```
2. set `inplace=True` 