# Prerequisites

## Dataset setup
1. `mkdir datasets && cd datasets`
2. ln -s `path/dataset/coco` coco 
3. Make directory as `Excluded` using PyCharm

## Compilation


## Training setup


## Evaluation setup
1. edit configs<br/>
`--config-file configs/name/config_name.yaml MODEL.WEIGHT weights/weight_name.pth TEST.IMS_PER_BATCH 1`