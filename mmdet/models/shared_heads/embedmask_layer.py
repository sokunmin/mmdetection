import torch.nn as nn
from mmcv.cnn import normal_init
from mmdet.core import auto_fp16, multi_apply
from mmdet.ops import ConvModule
from mmdet.ops.conv_module import LRASPP
from ..registry import SHARED_HEADS


@SHARED_HEADS.register_module
class MaskHeadShareLayer(nn.Module):

    def __init__(self,
                 in_channels,
                 feat_channels=256,
                 stacked_convs=1,
                 conv_cfg=None,
                 norm_cfg=dict(type='GN', num_groups=32, requires_grad=True),
                 norm_reg_target=True,
                 with_mask_branch=False,
                 add_fpn=False,
                 extra_cfg=None):
        super(MaskHeadShareLayer, self).__init__()
        self.in_channels = in_channels
        self.feat_channels = feat_channels
        self.stacked_convs = stacked_convs
        self.conv_cfg = conv_cfg
        self.norm_cfg = norm_cfg
        self.norm_reg_target = norm_reg_target
        self.extra_cfg = extra_cfg
        self.with_mask_branch = with_mask_branch
        self.add_fpn = add_fpn
        assert stacked_convs > 0
        self._init_layers()

    def _init_layers(self):
        self.cls_convs = nn.ModuleList()
        self.reg_convs = nn.ModuleList()
        if self.with_mask_branch:
            self.mask_convs = nn.ModuleList()

        if self.add_fpn:
            self.cls_fpn = nn.Conv2d(self.feat_channels, self.feat_channels, 1)
            self.reg_fpn = nn.Conv2d(self.feat_channels, self.feat_channels, 1)
            if self.with_mask_branch:
                self.mask_fpn = nn.Conv2d(self.feat_channels, self.feat_channels, 1)

        for i in range(self.stacked_convs):
            chn = self.in_channels if i == 0 else self.feat_channels
            if self.extra_cfg is not None and i == 0:
                self.cls_lraspp = self.build_lraspp_module(self.in_channels, self.feat_channels, self.extra_cfg)
                self.reg_lraspp = self.build_lraspp_module(self.in_channels, self.feat_channels, self.extra_cfg)
            else:
                self.cls_convs.append(
                    ConvModule(
                        chn,
                        self.feat_channels,
                        3,
                        stride=1,
                        padding=1,
                        conv_cfg=self.conv_cfg,
                        norm_cfg=self.norm_cfg,
                        bias=True if self.norm_reg_target else (self.norm_cfg is None)))
                self.reg_convs.append(
                    ConvModule(
                        chn,
                        self.feat_channels,
                        3,
                        stride=1,
                        padding=1,
                        conv_cfg=self.conv_cfg,
                        norm_cfg=self.norm_cfg,
                        bias=True if self.norm_reg_target else (self.norm_cfg is None)))
                if self.with_mask_branch:
                    self.mask_convs.append(
                        ConvModule(
                            chn,
                            self.feat_channels,
                            3,
                            stride=1,
                            padding=1,
                            conv_cfg=self.conv_cfg,
                            norm_cfg=self.norm_cfg,
                            bias=True if self.norm_reg_target else (self.norm_cfg is None)))

    def init_weights(self, pretrained=None):
        for i in range(len(self.cls_convs)):
            if i == 0 and self.extra_cfg is not None:
                self.cls_convs[i].init_weights()
                self.reg_convs[i].init_weights()
            else:
                normal_init(self.cls_convs[i].conv, std=0.01)
                normal_init(self.reg_convs[i].conv, std=0.01)
                if self.with_mask_branch:
                    normal_init(self.mask_convs[i].conv, std=0.01)
        if self.add_fpn:
            normal_init(self.cls_fpn, std=0.01)
            normal_init(self.reg_fpn, std=0.01)
            normal_init(self.mask_fpn, std=0.01)

    def build_lraspp_module(self, in_channels, out_channels, cfg):
        return LRASPP(in_channels, out_channels, **cfg)

    @auto_fp16()
    def forward(self, feats):
        return multi_apply(self.forward_single, feats, range(0, len(feats)))

    def forward_single(self, x, level):
        cls_feat = x
        reg_feat = x

        for cls_layer in self.cls_convs:
            cls_feat = cls_layer(cls_feat)

        for reg_layer in self.reg_convs:
            reg_feat = reg_layer(reg_feat)

        if self.add_fpn:
            # > MY-TODO: add 1x1 conv
            cls_feat = cls_feat + self.cls_fpn(x)
            reg_feat = reg_feat + self.reg_fpn(x)

        if self.with_mask_branch:
            if level == 0:
                mask_feat = x
                for mask_layer in self.mask_convs:
                    mask_feat = mask_layer(mask_feat)

                if self.add_fpn:
                    mask_feat = mask_feat + self.mask_fpn(x)
            else:
                mask_feat = None
            return cls_feat, reg_feat, mask_feat

        return cls_feat, reg_feat