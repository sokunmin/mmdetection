import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.modules.batchnorm import _BatchNorm
from mmcv.cnn import constant_init, xavier_init, kaiming_init, normal_init

from mmdet.ops import ConvModule, build_conv_layer, build_norm_layer, GeneralizedAttention
from mmdet.ops.activation import build_activation_layer
from mmdet.utils import get_root_logger
from ..registry import NECKS


class BasicBlock(nn.Module):
    expansion = 1

    def __init__(self,
                 inplanes,
                 planes,
                 stride=1,
                 downsample=None,
                 conv_cfg=None,
                 norm_cfg=dict(type='BN', requires_grad=True),
                 act_cfg=None,
                 gen_attention=None):
        super(BasicBlock, self).__init__()
        self.inplanes = inplanes
        self.planes = planes
        self.with_gen_attention = gen_attention is not None
        self.stride = stride
        self.downsample = downsample

        # 1x1 conv
        self.conv1 = build_conv_layer(
            conv_cfg,
            inplanes,
            planes // 2,
            1,
            stride=stride,
            bias=False
        )
        self.norm1_name, norm1 = build_norm_layer(norm_cfg, planes // 2, postfix=1)
        self.add_module(self.norm1_name, norm1)

        # 3x3 conv
        self.conv2 = build_conv_layer(
            conv_cfg,
            planes // 2,
            planes // 2,
            3,
            stride=stride,
            padding=1,
            bias=False
        )
        self.norm2_name, norm2 = build_norm_layer(norm_cfg, planes // 2, postfix=2)
        self.add_module(self.norm2_name, norm2)

        # 1x1 conv
        self.conv3 = build_conv_layer(
            conv_cfg,
            planes // 2,
            planes,
            1,
            stride=stride,
            bias=False
        )
        self.norm3_name, norm3 = build_norm_layer(norm_cfg, planes, postfix=3)
        self.add_module(self.norm3_name, norm3)

        self.relu = build_activation_layer(act_cfg)
        # gen_attention
        if self.with_gen_attention:
            self.gen_attention_block = GeneralizedAttention(
                planes, **gen_attention)

    @property
    def norm1(self):
        return getattr(self, self.norm1_name)

    @property
    def norm2(self):
        return getattr(self, self.norm2_name)

    @property
    def norm3(self):
        return getattr(self, self.norm3_name)

    def forward(self, x):
        identity = x

        out = self.conv1(x)
        out = self.norm1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.norm2(out)
        out = self.relu(out)

        if self.with_gen_attention:
            out = self.gen_attention_block(out)

        out = self.conv3(out)
        out = self.norm3(out)

        if self.downsample is not None:
            identity = self.downsample(x)

        out += identity
        out = self.relu(out)

        return out


def make_res_layer(block,
                   inplanes,
                   planes,
                   blocks,
                   stride=1,
                   conv_cfg=None,
                   norm_cfg=dict(type='BN', requires_grad=True),
                   act_cfg=None,
                   gen_attention=None,
                   gen_attention_blocks=[]):
    downsample = None
    if inplanes != planes * block.expansion:
        downsample = nn.Sequential(
            build_conv_layer(
                conv_cfg,
                inplanes,
                planes * block.expansion,
                kernel_size=1,
                stride=stride,
                bias=False),
            build_norm_layer(norm_cfg, planes * block.expansion)[1],
        )
    layers = []
    layers.append(
        block(
            inplanes=inplanes,
            planes=planes,
            stride=stride,
            downsample=downsample,
            conv_cfg=conv_cfg,
            norm_cfg=norm_cfg,
            act_cfg=act_cfg,
            gen_attention=gen_attention if
            (0 in gen_attention_blocks) else None))
    inplanes = planes * block.expansion
    for i in range(1, blocks):
        layers.append(
            block(
                inplanes=inplanes,
                planes=planes,
                stride=1,
                conv_cfg=conv_cfg,
                norm_cfg=norm_cfg,
                act_cfg=act_cfg,
                gen_attention=gen_attention if
                (i in gen_attention_blocks) else None))
    return nn.Sequential(*layers)


class SELayer(nn.Module):

    def __init__(self, inplanes, reduction=16):
        super(SELayer, self).__init__()
        self.avg_pool = nn.AdaptiveAvgPool2d(1)
        self.fc = nn.Sequential(
            nn.Linear(inplanes, inplanes // reduction),
            nn.LeakyReLU(inplace=True),
            nn.Linear(inplanes // reduction, inplanes),
            nn.Sigmoid())

    def forward(self, x):
        b, c, _, _ = x.size()
        y = self.avg_pool(x).view(b, c)
        y = self.fc(y).view(b, c, 1, 1)
        return x * y


class Hourglass(nn.Module):

    def __init__(self,
                 depth,
                 inplanes,
                 planes,
                 feat_channel=128,
                 conv_cfg=None,
                 norm_cfg=dict(type='BN', requires_grad=True),
                 act_cfg=None):
        super(Hourglass, self).__init__()
        self.depth = depth
        self.inplanes = inplanes
        self.planes = planes
        self.feat_channel = feat_channel
        self.conv_cfg = conv_cfg
        self.norm_cfg = norm_cfg
        self.act_cfg = act_cfg
        self.hg = self._make_hourglass()
        self.downsample = nn.MaxPool2d(2, 2)
        self.upsample = nn.Upsample(scale_factor=2, mode='nearest')

    def _make_hourglass(self):
        hg = nn.ModuleList()
        for i in range(self.depth):  # > #depth
            #  skip path; up_residual_block; down_residual_block_path,
            # 0 ~ n-2 (except the outermost n-1 order) need 3 residual blocks
            res = self._make_lower_residual(i)
            if i == (self.depth - 1):  # the deepest path (i.e. the longest path) need 4 residual blocks
                res.append(self._make_single_residual(i))
            hg.append(res)  # pack conv layers of every oder of hourglass block
        return hg

    def _make_single_residual(self, depth_id):
        return make_res_layer(
            BasicBlock,
            self.inplanes + self.feat_channel * (depth_id + 1),
            self.inplanes + self.feat_channel * (depth_id + 1),
            1,
            conv_cfg=self.conv_cfg,
            norm_cfg=self.norm_cfg,
            act_cfg=self.act_cfg)  # idx: 4

    def _make_lower_residual(self, depth_id):
        # [0]: 256, [1]: 384, [2]: 512, [3]: 640, [4]: 768
        layers = [
            make_res_layer(
                BasicBlock,
                self.inplanes + self.feat_channel * depth_id,
                self.inplanes + self.feat_channel * depth_id,
                1,
                conv_cfg=self.conv_cfg,
                norm_cfg=self.norm_cfg,
                act_cfg=self.act_cfg),  # > idx: 0
            make_res_layer(
                BasicBlock,
                self.inplanes + self.feat_channel * depth_id,
                self.inplanes + self.feat_channel * (depth_id + 1),
                1,
                conv_cfg=self.conv_cfg,
                norm_cfg=self.norm_cfg,
                act_cfg=self.act_cfg),  # > idx: 1
            make_res_layer(
                BasicBlock,
                self.inplanes + self.feat_channel * (depth_id + 1),
                self.inplanes + self.feat_channel * depth_id,
                1,
                conv_cfg=self.conv_cfg,
                norm_cfg=self.norm_cfg,
                act_cfg=self.act_cfg),  # > idx: 2
            ConvModule(
                self.inplanes + self.feat_channel * depth_id,
                self.inplanes + self.feat_channel * depth_id,
                3,
                padding=1,
                norm_cfg=self.norm_cfg,
                act_cfg=self.act_cfg,
                bias=False)  # > idx: 3
        ]
        return nn.ModuleList(layers)

    def forward_single(self, depth_id, x, up_feats):
        up1 = self.hg[depth_id][0](x)
        down1 = self.downsample(x)
        down1 = self.hg[depth_id][1](down1)
        if depth_id == (self.depth - 1):  # except for the highest-order hourglass block
            down2 = self.hg[depth_id][4](down1)
        else:
            # call the lower-order hourglass block recursively
            down2 = self.forward_single(depth_id + 1, down1, up_feats)
        up_feats.append(down2)
        down3 = self.hg[depth_id][2](down2)
        up2 = self.upsample(down3)
        deconv1 = self.hg[depth_id][3](up2)  # TOCHECK: dim is different?
        return up1 + deconv1

    def forward(self, x):
        up_feats = []  # collect feature maps produced by low2 at every scale
        feats = self.forward_single(0, x, up_feats)
        return [feats] + up_feats[::-1]


@NECKS.register_module
class IMHFPN(nn.Module):
    """Identity Mapping Hourgalss Feature Pyramid Network (IMH FPN)

    Args:
        in_channels (list): number of channels for each branch.
        out_channels (int): output channels of feature pyramids.
        num_outs (int): number of output stages.
        pooling_type (str): pooling for generating feature pyramids
            from {MAX, AVG}.
        conv_cfg (dict): dictionary to construct and config conv layer.
        norm_cfg (dict): dictionary to construct and config norm layer.
        with_cp  (bool): Use checkpoint or not. Using checkpoint will save some
            memory while slowing down the training speed.
        stride (int): stride of 3x3 convolutional layers
    """

    def __init__(self,
                 in_channels,
                 out_channels,
                 num_stages=4,
                 num_outs=5,
                 depth=4,
                 feat_channel=128,
                 pooling_type='AVG',
                 conv_cfg=None,
                 norm_cfg=dict(type='BN', requires_grad=True),
                 act_cfg=None,
                 with_cp=False,
                 stride=1,
                 zero_init_residual=True):
        super(IMHFPN, self).__init__()
        assert isinstance(in_channels, (int, list))
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.num_scales = len(in_channels) if isinstance(in_channels, list) else num_outs
        self.num_stages = num_stages
        self.num_outs = num_outs
        self.feat_channel = feat_channel
        self.with_cp = with_cp
        self.conv_cfg = conv_cfg
        self.norm_cfg = norm_cfg
        self.zero_init_residual = zero_init_residual
        self.hg_blocks = nn.ModuleList()
        self.se_convs = nn.ModuleList()
        self.out_convs = nn.ModuleList()
        self.cache_feat = nn.ModuleList()
        self.cache_out = nn.ModuleList()
        for t in range(self.num_stages):
            self.hg_blocks.append(
                Hourglass(
                    depth=depth,
                    inplanes=in_channels,
                    planes=out_channels,
                    feat_channel=feat_channel,
                    conv_cfg=conv_cfg,
                    norm_cfg=norm_cfg,
                    act_cfg=act_cfg)
            )
            self.se_convs.append(nn.ModuleList([nn.Sequential(
                ConvModule(
                    in_channels + i * feat_channel,
                    in_channels,
                    3,
                    padding=1,
                    norm_cfg=norm_cfg,
                    act_cfg=act_cfg,
                    bias=False),
                ConvModule(
                    in_channels,
                    in_channels,
                    3,
                    padding=1,
                    norm_cfg=norm_cfg,
                    act_cfg=act_cfg,
                    bias=False),
                SELayer(in_channels)) for i in range(self.num_scales)]))
            # > MY TODO: move to `keypoint_head`
            self.out_channels = 50
            self.out_convs.append(nn.ModuleList([
                nn.Conv2d(
                    in_channels,
                    out_channels,  # MY TODO: change to `out_channels`
                    kernel_size=1,
                    bias=True) for i in range(self.num_scales)]))
            if t < num_stages - 1:
                self.cache_feat.append(nn.ModuleList([
                    ConvModule(
                        in_channels,
                        in_channels + i * feat_channel,
                        1,
                        norm_cfg=norm_cfg,
                        act_cfg=None,
                        bias=False) for i in range(self.num_scales)]))
                self.cache_out.append(nn.ModuleList([
                    ConvModule(
                        out_channels,
                        in_channels + i * feat_channel,
                        1,
                        norm_cfg=norm_cfg,
                        act_cfg=None,
                        bias=False) for i in range(self.num_scales)]))

    def init_weights(self, pretrained=None):
        if isinstance(pretrained, str):
            logger = get_root_logger()
            # load_checkpoint(self, pretrained, strict=False, logger=logger)
        elif pretrained is None:
            for m in self.modules():
                if isinstance(m, nn.Conv2d):
                    # xavier_init(m, distribution='uniform')
                    # normal_init(m, mean=0, std=0.001)
                    kaiming_init(m)
                elif isinstance(m, (_BatchNorm, nn.GroupNorm)):
                    constant_init(m, 1)
                elif isinstance(m, nn.Linear):
                    normal_init(m, mean=0, std=0.01)

            if self.zero_init_residual:
                for m in self.modules():
                    if isinstance(m, BasicBlock):
                        constant_init(m.norm3, 0)
                    # elif isinstance(m, BasicBlock):
                    #     constant_init(m.norm2, 0)
        else:
            raise TypeError('pretrained must be a str or None')

    def _create_caches(self, x):
        assert len(x) == self.num_scales
        return [torch.zeros_like(x[s]) for s in range(self.num_scales)]

    def forward(self, x):
        outs = []
        hg_caches = None
        for t, hg_block, se_convs, out_convs in \
            zip(range(self.num_stages), self.hg_blocks, self.se_convs, self.out_convs):  # > #stages
            hg_outs = hg_block(x)  # -> (#scales,): (0:256, 1:384, 2:512, 3:640, 4:786)
            if t == 0:
                hg_caches = self._create_caches(hg_outs)
            _outs = []
            for s, hg_out, hg_cache, se_conv, out_conv in \
                    zip(range(self.num_scales), hg_outs, hg_caches, se_convs, out_convs):  # > #scales
                if t > 0:
                    hg_out = hg_out + hg_cache
                hg_out = se_conv(hg_out)
                out = out_conv(hg_out)
                _outs.append(out)
                if t != self.num_stages - 1:  # > save new cache
                    new_cache = self.cache_feat[t][s](hg_out) + self.cache_out[t][s](out)
                    if s == 0:
                        x = x + new_cache
                    hg_caches[s] = new_cache
            outs.append(_outs)
        return tuple(outs)
