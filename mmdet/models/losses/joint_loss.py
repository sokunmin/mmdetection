import torch
import torch.nn as nn
import torch.nn.functional as F

from ..registry import LOSSES
from .utils import weighted_loss, weight_reduce_loss

mse_loss = weighted_loss(F.mse_loss)


@LOSSES.register_module
class JointsMSELoss(nn.Module):

    def __init__(self, use_target_weight, reduction='mean', loss_weight=0.5):
        super(JointsMSELoss, self).__init__()
        self.reduction = reduction
        self.use_target_weight = use_target_weight
        self.loss_weight = loss_weight

    def forward(self, pred, target, target_weight, avg_factor=None):
        batch_size = pred.size(0)
        num_joints = pred.size(1)
        heatmaps_pred = pred.reshape((batch_size, num_joints, -1)).split(1, 1)
        heatmaps_gt = target.reshape((batch_size, num_joints, -1)).split(1, 1)
        loss = 0

        for idx in range(num_joints):
            heatmap_pred = heatmaps_pred[idx].squeeze()
            heatmap_gt = heatmaps_gt[idx].squeeze()
            mse_loss = F.mse_loss(
                heatmap_pred.mul(target_weight[:, idx]),
                heatmap_gt.mul(target_weight[:, idx]),
                reduction='none'
            )
            loss += self.loss_weight * weight_reduce_loss(
                mse_loss, None, self.reduction, avg_factor
            )
            # [original]
            # if self.use_target_weight:
            #     loss += self.loss_weight * self.criterion(
            #         heatmap_pred.mul(target_weight[:, idx]),
            #         heatmap_gt.mul(target_weight[:, idx])
            #     )
            # else:
            #     loss += self.loss_weight * self.criterion(heatmap_pred, heatmap_gt)

        return loss / num_joints


@LOSSES.register_module
class JointsOHKMMSELoss(nn.Module):
    def __init__(self, use_target_weight, topk=8, reduction='mean', loss_weight=0.5):
        super(JointsOHKMMSELoss, self).__init__()
        self.use_target_weight = use_target_weight
        self.topk = topk
        self.reduction = reduction
        self.loss_weight = loss_weight

    def ohkm(self, loss):
        ohkm_loss = 0.
        for i in range(loss.size()[0]):
            sub_loss = loss[i]
            topk_val, topk_idx = torch.topk(
                sub_loss, k=self.topk, dim=0, sorted=False
            )
            tmp_loss = torch.gather(sub_loss, 0, topk_idx)
            ohkm_loss += torch.sum(tmp_loss) / self.topk
        ohkm_loss /= loss.size()[0]
        return ohkm_loss

    def forward(self, pred, target, target_weight, avg_factor=None):
        batch_size = pred.size(0)
        num_joints = pred.size(1)
        heatmaps_pred = pred.reshape((batch_size, num_joints, -1)).split(1, 1)
        heatmaps_gt = target.reshape((batch_size, num_joints, -1)).split(1, 1)

        loss = []
        for idx in range(num_joints):
            heatmap_pred = heatmaps_pred[idx].squeeze()
            heatmap_gt = heatmaps_gt[idx].squeeze()
            mse_loss = F.mse_loss(
                heatmap_pred.mul(target_weight[:, idx]),
                heatmap_gt.mul(target_weight[:, idx]),
                reduction='none'
            )
            loss.append(self.loss_weight * weight_reduce_loss(
                mse_loss, None, self.reduction, avg_factor
            ))
            # if self.use_target_weight:
            #     loss.append(self.loss_weight * self.criterion(
            #         heatmap_pred.mul(target_weight[:, idx]),
            #         heatmap_gt.mul(target_weight[:, idx])
            #     ))
            # else:
            #     loss.append(
            #         self.loss_weight * self.criterion(heatmap_pred, heatmap_gt)
            #     )

        loss = [l.mean(dim=1).unsqueeze(dim=1) for l in loss]
        loss = torch.cat(loss, dim=1)

        return self.ohkm(loss)
