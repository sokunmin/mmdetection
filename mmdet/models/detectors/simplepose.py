import numpy as np
from ..registry import DETECTORS
from .single_stage_mt import SingleStageMultiDetector
from ...datasets import CocoPersonDataset


@DETECTORS.register_module
class SimplePose(SingleStageMultiDetector):

    BODYPART_PAIRS = [
        [1, 0], [1, 14], [1, 15], [1, 16], [1, 17],
        [0, 14], [0, 15], [14, 16], [15, 17], [1, 2],
        [2, 3], [3, 4], [1, 5], [5, 6], [6, 7],
        [1, 8], [8, 9], [9, 10], [1, 11], [11, 12],
        [12, 13], [0, 2], [0, 5], [2, 8], [8, 12],
        [5, 11], [11, 9], [16, 2], [17, 5], [8, 11],
    ]
    MAP_ORDERS = [0, 15, 14, 17, 16, 5, 2, 6, 3, 7, 4, 11, 8, 12, 9, 13, 10, 1]

    def __init__(self,
                 backbone,
                 neck=None,
                 shared_head=None,
                 bbox_head=None,
                 mask_head=None,
                 keypoint_head=None,
                 train_cfg=None,
                 test_cfg=None,
                 pretrained=None,
                 loss_auto_weighted=None,):
        super(SimplePose, self).__init__(backbone, neck, shared_head, bbox_head, mask_head,
                                         keypoint_head, train_cfg, test_cfg, pretrained,
                                         loss_auto_weighted)

    def group_inputs(self, *inputs, key='bbox', return_loss=False):
        if return_loss:
            outs, gt_keypoints, gt_labels, img_metas, cfg = inputs
            inputs = (outs, gt_keypoints, gt_labels, img_metas, cfg, SimplePose.BODYPART_PAIRS)
        else:
            outs, img_metas, cfg, rescale = inputs
            num_bodyparts = len(SimplePose.BODYPART_PAIRS)
            joint_outs = outs[-1][0][:, num_bodyparts:]
            bodypart_outs = outs[-1][0][:, :num_bodyparts]
            inputs = (joint_outs, bodypart_outs, img_metas, cfg, rescale, SimplePose.BODYPART_PAIRS)
        return inputs