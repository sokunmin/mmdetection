from ..registry import DETECTORS
from .single_stage import SingleStageDetector
from .single_stage_mt import SingleStageMultiDetector


@DETECTORS.register_module
class FCOS(SingleStageDetector):

    def __init__(self,
                 backbone,
                 neck,
                 bbox_head,
                 train_cfg=None,
                 test_cfg=None,
                 pretrained=None):
        super(FCOS, self).__init__(backbone, neck, bbox_head, train_cfg,
                                   test_cfg, pretrained)


@DETECTORS.register_module
class FCOSImprv(SingleStageMultiDetector):

    def __init__(self,
                 backbone,
                 neck,
                 bbox_head,
                 shared_head=None,
                 mask_head=None,
                 keypoint_head=None,
                 train_cfg=None,
                 test_cfg=None,
                 pretrained=None,
                 loss_auto_weighted=None):
        super(FCOSImprv, self).__init__(backbone, neck, shared_head, bbox_head, mask_head,
                                        keypoint_head, train_cfg, test_cfg, pretrained,
                                        loss_auto_weighted)

    def group_losses(self, losses):
        loss_d, loss_c = [], []
        for name, value in losses.items():
            if name == 'loss_cls' or name == 'loss_centerness':
                loss_d.append(value)
            elif name == 'loss_bbox':
                loss_c.append(value)
            else:
                raise KeyError('No key named', name, ' found')
        return loss_d, loss_c

    def loss(self, pred_outs, gt_inputs, img_metas, gt_bboxes_ignore=None):
        gt_bboxes, gt_masks, gt_keypoints, gt_labels = gt_inputs
        loss_inputs = pred_outs['bbox'] + (gt_bboxes, gt_masks, gt_labels, img_metas, self.train_cfg)
        all_losses = {}
        bbox_losses = self.bbox_head.loss(
            *loss_inputs, gt_bboxes_ignore=gt_bboxes_ignore)
        all_losses.update(bbox_losses)
        if self.with_mask:
            loss_inputs = pred_outs['mask'] + (gt_masks, gt_labels, img_metas, self.train_cfg)
            mask_losses = self.mask_head.loss(
                *loss_inputs, gt_bboxes_ignore=gt_bboxes_ignore)
            all_losses.update(mask_losses)
        return all_losses