from ..registry import DETECTORS
from .single_stage_mt import SingleStageMultiDetector
from mmdet.core import bbox2result
from mmdet.core import multitask_nms


@DETECTORS.register_module
class EmbedMask(SingleStageMultiDetector):

    def __init__(self,
                 backbone,
                 neck,
                 shared_head=None,
                 bbox_head=None,
                 mask_head=None,
                 keypoint_head=None,
                 train_cfg=None,
                 test_cfg=None,
                 pretrained=None,
                 loss_auto_weighted=None,
                 mask_in_share=False):
        super(EmbedMask, self).__init__(backbone, neck, shared_head, bbox_head, mask_head,
                                        keypoint_head, train_cfg, test_cfg, pretrained,
                                        loss_auto_weighted)
        self.mask_in_share = mask_in_share

    def group_feat(self, x, outs=None, key='bbox'):
        """
        Parameters:
        -----------
            x: fpn features for all levels
            outs: {'bbox':[ cls_outs, reg_outs, centerness_outs, cls_feats, reg_feats ]}
        """

        if key == 'fpn':
            if self.with_shared_head:
                input_feat = []
                for lvl in range(len(x[0])):
                    cls_feat = x[0][lvl]
                    reg_feat = x[1][lvl]
                    if self.mask_in_share:
                        # > `with_mask_branch`
                        mask_feat = x[2][lvl]
                        input_feat.append((cls_feat, reg_feat, mask_feat))
                    else:
                        input_feat.append((cls_feat, reg_feat))
                x = input_feat
                return x
            return x, outs
        elif key == 'bbox' and key in outs:
            bbox_outs = outs[key]  # > from `fcos_head`
            if self.with_shared_head:
                # > MY-TODO: need to modify for keypoint input
                input_feat = []
                for lvl in range(len(x)):  # > `x` is from `shared_head`
                    input_feat.append(x[lvl][-1])
                x = input_feat
            # > reg_outs & reg_feats
            x = (x, tuple(bbox_outs[1]), tuple(bbox_outs[-1]))
            outs[key] = bbox_outs[:3]
        return x, outs

    def loss(self, pred_outs, gt_inputs, img_metas, gt_bboxes_ignore=None):
        bbox_outs, mask_outs = pred_outs['bbox'], pred_outs['mask']
        gt_bboxes, gt_masks, gt_keypoints, gt_labels = gt_inputs
        all_losses = {}
        # > bbox loss
        loss_inputs = bbox_outs + (gt_bboxes, gt_masks, gt_labels, img_metas, self.train_cfg)
        bbox_losses = self.bbox_head.loss(
            *loss_inputs, gt_bboxes_ignore=gt_bboxes_ignore)
        all_losses.update(bbox_losses)
        # > mask loss
        bbox_preds = tuple([bbox_outs[1]])
        loss_inputs = (bbox_preds + mask_outs) + (gt_bboxes, gt_masks, gt_labels, img_metas, self.train_cfg)
        mask_losses = self.mask_head.loss(
            *loss_inputs, gt_bboxes_ignore=gt_bboxes_ignore)
        all_losses.update(mask_losses)
        return all_losses

    def simple_test(self, img, img_meta, rescale=False):
        x = self.extract_feat(img)
        if self.with_shared_head:
            x = self.shared_head(x)
            x = self.group_feat(x, key='fpn')
        # > bbox: cls_score, bbox_preds, centerness, cls_feat, reg_feat
        bbox_outs = self.bbox_head(x)
        all_outs = dict(bbox=bbox_outs)
        x, all_outs = self.group_feat(x, all_outs, key='bbox')
        bbox_inputs = all_outs['bbox'] + (img_meta, self.test_cfg, rescale)
        mlvl_bboxes, mlvl_labels, mlvl_factors, mlvl_topk_inds = self.bbox_head.get_bboxes(
            *bbox_inputs, nms=False)[0]

        # > mask: proposal_embeds, proposal_margins, pixel_embeds
        mask_outs = self.mask_head(x)
        mask_inputs = mask_outs + (mlvl_topk_inds, img_meta, self.test_cfg, rescale)
        mlvl_proposal_embeds, mlvl_proposal_margins, all_pixel_embeds = self.mask_head.get_masks(
            *mask_inputs)[0]
        # > nms
        mlvl_other_tasks = dict(
            proposal_embeds=mlvl_proposal_embeds,
            proposal_margins=mlvl_proposal_margins
        )
        # > len(det_bboxes): 5, (bboxes, scores)
        det_bboxes, det_labels, det_tasks = multitask_nms(
            mlvl_bboxes,
            mlvl_labels,
            mlvl_other_tasks,
            self.test_cfg.score_thr,
            self.test_cfg.nms,
            self.test_cfg.max_per_img,
            score_factors=mlvl_factors,
            extra_cfg=dict(
                proposal_embeds=mlvl_proposal_embeds.size()[-1],
                proposal_margins=mlvl_proposal_margins.size()[-1]
            ))
        bbox_results = bbox2result(det_bboxes, det_labels, self.bbox_head.num_classes)
        if det_bboxes.shape[0] == 0:
            segm_results = [[] for _ in range(self.mask_head.num_classes - 1)]
        else:
            det_proposal_embeds = det_tasks['proposal_embeds'].unsqueeze(0)
            det_proposal_margins = det_tasks['proposal_margins'].unsqueeze(0)
            scale_factor = img_meta[0]['scale_factor']
            pixel_mask_size = all_pixel_embeds.size()[-2:]
            pixel_embeds, bbox_targets = self.mask_head.embedmask_mask_target(
                all_pixel_embeds, det_bboxes, pixel_mask_size, rescale=rescale, scale_factor=scale_factor)

            _, det_masks = self.mask_head.get_pixel_embeds(
                pixel_embeds, det_proposal_embeds, det_proposal_margins,
                bbox_targets.unsqueeze(0))

            segm_results = self.mask_head.get_seg_masks(det_masks[0], det_labels,
                                                        self.test_cfg, img_meta,
                                                        rescale, scale_factor)

        return bbox_results, segm_results