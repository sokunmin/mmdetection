import torch
import torch.nn as nn
import torch.utils.checkpoint as cp
from mmcv.cnn import constant_init, kaiming_init, normal_init
from mmcv.runner import load_checkpoint
from torch.nn.modules.batchnorm import _BatchNorm

from mmdet.models.necks.imh_fpn import make_res_layer, BasicBlock
from mmdet.ops import (ContextBlock, GeneralizedAttention, build_conv_layer,
                       build_norm_layer, ConvModule)
from mmdet.utils import get_root_logger
from ..registry import BACKBONES


class DilatedConv(nn.Module):

    def __init__(self,
                 inplanes,
                 planes,
                 stride=1,
                 dilation=1,
                 norm_cfg=dict(type='BN'),
                 act_cfg=dict(
                     type='ReLU',
                     inplace=True)):
        super(DilatedConv, self).__init__()
        self.inplanes = inplanes
        self.conv = ConvModule(
            inplanes,
            planes,
            3,
            stride=stride,
            padding=dilation,
            dilation=dilation,
            norm_cfg=norm_cfg,
            act_cfg=act_cfg,
            bias=False
        )

    def forward(self, x):
        assert x.size()[1] == self.inplanes, "input channel {} dese not fit kernel channel {}".format(x.size()[1], self.inp_dim)
        out = self.conv(x)
        return out


@BACKBONES.register_module
class PRM(nn.Module):
    """Pyramid Residual Module (PRMs)"""
    def __init__(self,
                 inplanes=3,
                 feat_channels=128,
                 dilations=(3, 3, 4, 4, 5, 5),
                 conv_cfg=None,
                 norm_cfg=dict(type='BN'),
                 act_cfg=dict(
                     type='ReLU',
                     inplace=True)):
        super(PRM, self).__init__()
        self.inplanes = inplanes
        self.dilations = dilations
        self.conv1 = ConvModule(
            inplanes,
            feat_channels // 2,
            7,
            stride=2,
            padding=3,
            norm_cfg=norm_cfg,
            act_cfg=act_cfg,
            bias=False
        )

        self.res_layer1 = make_res_layer(
            BasicBlock,
            feat_channels // 2,
            feat_channels,
            1,
            conv_cfg=conv_cfg,
            norm_cfg=norm_cfg,
            act_cfg=act_cfg)
        self.res_layer2 = make_res_layer(
            BasicBlock,
            feat_channels,
            feat_channels,
            1,
            conv_cfg=conv_cfg,
            norm_cfg=norm_cfg,
            act_cfg=act_cfg)

        self.pool = nn.MaxPool2d(2, 2)

        dilated_convs = []
        for dilation in dilations:
            dilated_convs.append(DilatedConv(feat_channels,
                                             feat_channels,
                                             dilation=dilation,
                                             norm_cfg=norm_cfg,
                                             act_cfg=act_cfg))

        self.dilated_convs = nn.Sequential(*dilated_convs)

    def init_weights(self, pretrained=None):
        if isinstance(pretrained, str):
            logger = get_root_logger()
            # load_checkpoint(self, pretrained, strict=False, logger=logger)
        elif pretrained is None:
            for m in self.modules():
                if isinstance(m, nn.Conv2d):
                    kaiming_init(m)
                    # normal_init(m, mean=0, std=0.001)
                elif isinstance(m, (_BatchNorm, nn.GroupNorm)):
                    constant_init(m, 1)
        else:
            raise TypeError('pretrained must be a str or None')

    def forward(self, x):
        out = self.conv1(x)
        out = self.res_layer1(out)
        out = self.pool(out)
        out = self.res_layer2(out)
        dilated_out = self.dilated_convs(out)
        outs = torch.cat([out, dilated_out], dim=1)  # (N, C1+C2, H, W)
        return outs