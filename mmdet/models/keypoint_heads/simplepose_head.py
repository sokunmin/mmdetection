import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from mmcv.cnn import normal_init
import mmcv
import math
import cv2
import matplotlib.pyplot as plt
from mmdet.ops import build_conv_layer, ConvModule
from ..registry import HEADS
from mmdet.core import force_fp32, multi_apply, multiclass_nms, cmu_heatmap_nms
from ..builder import build_loss
from ...core.keypoint.cmu_skeleton import SingleJoint, SingleLimb, SingleSkeleton
from ...core.post_processing.bbox_nms import cmu_heatmap_nms_cpu
from ...ops.openpose_paf import pafprocess as pafprc

USE_CPU = True
FOR_OFFICIAL_MODEL = True


def compute_resized_coords(coords, scale_factor):
    return (coords + 0.5) * scale_factor - 0.5


def compute_resized_coords_cpu(coords, scale_factor):
    return (np.array(coords, dtype=np.float32) + 0.5) * scale_factor - 0.5


def compute_gaussian_distance(sigma, x, u):
    variance = 2 * sigma ** 2
    y = (- (x - u) ** 2 / variance).exp()
    return y


@HEADS.register_module
class SimplePoseHead(nn.Module):

    def __init__(self,
                 num_classes,
                 in_channels,
                 stacked_convs=4,
                 target_type='gaussian',
                 feat_channels=128,
                 strides=[4, 8, 16, 32, 64],
                 scales=(0.5, 1., 1.5, 2., 3.),
                 joint_cfg=dict(num_joints=18,  # 18 + 2 (bg)
                                num_bg=2,
                                sigma=2,
                                thr=0.1),
                 bodypart_cfg=dict(num_bodyparts=30,
                                   intermd_points=20,
                                   min_intermd_factor=0.8,
                                   thr=0.1,
                                   weights=[0.5, 0.25, 0.25]),
                 skeleton_cfg=dict(min_length_ratio=16,
                                   min_score_ratio=0.7,
                                   delete_shared_joints=False),
                 gen_cfg=dict(
                     joint_sigma=9,
                     bodypart_sigma=7,
                     joint_thr=0.015,
                     bodypart_thr=0.015
                 ),
                 loss_joint=None,
                 conv_cfg=None,
                 norm_cfg=dict(type='BN', requires_grad=True),
                 act_cfg=None):
        super(SimplePoseHead, self).__init__()
        self.num_classes = num_classes
        self.in_channels = in_channels
        self.num_scales = len(in_channels) if isinstance(in_channels, list) else 5
        self.stacked_convs = stacked_convs
        self.strides = strides
        self.scales = scales
        self.target_type = target_type
        self.feat_channels = feat_channels
        self.joint_cfg = joint_cfg
        self.bodypart_cfg = bodypart_cfg
        self.skeleton_cfg = skeleton_cfg
        self.gen_cfg = gen_cfg
        self.loss_cfg = loss_joint
        self.loss_joint = build_loss(loss_joint)
        self.conv_cfg = conv_cfg
        self.norm_cfg = norm_cfg
        self.act_cfg = act_cfg
        self.fp16_enabled = False
        self.num_joints = self.joint_cfg['num_joints']
        self.num_joint_bg = self.joint_cfg['num_bg']
        self.num_bodyparts = self.bodypart_cfg['num_bodyparts']
        self._init_layers()

    def _init_layers(self):
        pass

    def init_weights(self):
        pass

    def forward(self, feats):
        # outs = multi_apply(self.forward_single, feats)
        num_joint_outs = self.num_joints + self.num_joint_bg
        return feats

    def forward_single(self, x):
        num_joint_outs = self.num_joints + self.num_joint_bg
        heatmap_preds = x[0][:, self.num_bodyparts:num_joint_outs]
        paf_preds = x[0][:, :self.num_bodyparts]
        return heatmap_preds, paf_preds

    @force_fp32(apply_to=('preds'))
    def loss(self,
             preds,
             gt_keypoints,
             gt_labels,
             img_metas,
             cfg,
             bodypart_pairs,
             gt_bboxes_ignore=None,
             gt_masks_ignore=None,
             gt_keypoints_ignore=None):
        # MY-TODO: add multi-level
        featmap_sizes = [featmap.size()[-2:] for featmap in preds[0]]
        dtype, device = preds[0][0].dtype, preds[0][0].device
        all_level_points = self.get_points(featmap_sizes, dtype, device)
        pose_targets = self.simplepose_target(all_level_points, gt_keypoints, gt_masks_ignore,
                                              img_metas, bodypart_pairs, featmap_sizes, dtype, device)  # (B, 50, H, W)
        num_img = len(img_metas)
        num_stacks = len(preds)
        num_joints = self.joint_cfg['num_joints']
        loss_weights = self.loss_cfg['scale_weights']
        gt_masks_all, gt_masks_miss = gt_masks_ignore
        gt_masks_miss = gt_masks_miss.transpose((1, 2, 0))  # (H, W, B)

        # # > NOTE: `debug` only
        # import pickle
        # with open('dummy/p_t_dump.pkl', 'rb') as f:
        #     dump_info = pickle.load(f)
        #     test_preds = dump_info['p']  # (#stack, #scale, B, 50, H, W)
        #     test_targets = dump_info['t']  # (#mask, B, 1, H, W)
        #     preds = test_preds
        #     masks_miss, pose_targets = test_targets
        #     masks_miss = masks_miss.squeeze().cpu().numpy().transpose((1, 2, 0))
        #     gt_masks_miss = masks_miss

        losses = []
        for i_lvl, featmap_size, loss_weight in zip(range(len(featmap_sizes)), featmap_sizes, loss_weights):
            lvl_preds = [pred[i_lvl][None, :] for pred in preds]
            lvl_preds = torch.cat(lvl_preds, dim=0)  # (#stack, B, C, H, W)
            pose_targets = F.adaptive_avg_pool2d(pose_targets, output_size=featmap_size)  # (B, 50, H, W)
            mask_targets = mmcv.imresize(gt_masks_miss, size=featmap_size).transpose((2, 0, 1))  # (B, H, W)
            # mask_targets = F.interpolate(gt_masks_miss.unsqueeze(1), size=featmap_size, mode='bilinear')
            mask_targets[mask_targets < 0.5] = 0
            mask_targets = np.repeat(mask_targets[:, None], pose_targets.size(1), axis=1)  # (B, 50, H, W)
            mask_targets[:, num_joints] *= self.gen_cfg['bg_weight']  # bg channel 1 = gt_masks_all
            mask_targets[:, :num_joints] *= self.gen_cfg['joint_weight']  # joint channels
            mask_targets = torch.from_numpy(mask_targets).to(device)  # (B, 50, H, W)
            loss = loss_weight * self.loss_joint(lvl_preds,
                                                 pose_targets[None, :],
                                                 weight=mask_targets[None, :],
                                                 avg_factor=num_stacks)
            losses.append(loss)
        loss_all = sum(losses) / sum(loss_weights) / num_img
        return dict(loss_all=loss_all)

    @force_fp32(apply_to=('joint_preds', 'bodypart_preds'))
    def get_keypoints(self,
                      joint_preds,
                      bodypart_preds,
                      img_metas,
                      cfg,
                      rescale=False,
                      bodypart_pairs=None):
        # > gpu
        if not USE_CPU:
            bodypart_preds = F.interpolate(bodypart_preds, scale_factor=self.strides[0], mode='bicubic')
        else:
            # > cpu
            joint_preds = joint_preds.cpu().numpy().transpose((0, 2, 3, 1))
            bodypart_preds = F.interpolate(bodypart_preds, scale_factor=self.strides[0], mode='bicubic').cpu().numpy().transpose((0, 2, 3, 1))

        result_list = []
        for img_id, joint_pred, bodypart_pred, img_meta in \
                zip(range(len(img_metas)), joint_preds, bodypart_preds, img_metas):
            all_joints = self.get_joints(joint_pred)
            if not USE_CPU:
                all_bodyparts = self.get_bodyparts(bodypart_pred, all_joints, img_meta['img_shape'], bodypart_pairs)
                all_joints = [j for js in all_joints for j in js]
                all_skeletons = self.get_skeletons(all_joints, all_bodyparts, bodypart_pairs)
            else:
                all_joints = [j for js in all_joints for j in js]
                all_skeletons = self.get_skeletons_cpu(bodypart_pred, all_joints, img_meta['img_shape'])
            result_list.append(all_skeletons)
        return result_list

    def get_joints(self, joint_preds):
        if not USE_CPU:
            device = joint_preds.get_device()
        total_joint_count = 0
        all_joints = []
        sigma = self.joint_cfg['sigma']
        for joint_type in range(self.num_joints):
            if not USE_CPU:
                joints = []
                joint_pred = joint_preds[joint_type, :, :]
                joint_coords = cmu_heatmap_nms(joint_pred, self.joint_cfg['thr'])
                l = (joint_coords[:, 0] - sigma).clamp(min=0)  # (4,)
                t = (joint_coords[:, 1] - sigma).clamp(min=0)
                r = (joint_coords[:, 0] + sigma).clamp(max=joint_pred.size(1) - 1)
                b = (joint_coords[:, 1] + sigma).clamp(max=joint_pred.size(0) - 1)
            else:
                joint_pred = joint_preds[:, :, joint_type]
                joint_coords = cmu_heatmap_nms_cpu(joint_pred, self.joint_cfg['thr'])
                joints = np.zeros((len(joint_coords), 5), dtype=np.float32)  # > (#peaks, (x,y,score,peak_id))
                l = (joint_coords[:, 0] - sigma).clip(min=0)  # (4,)
                t = (joint_coords[:, 1] - sigma).clip(min=0)
                r = (joint_coords[:, 0] + sigma).clip(max=joint_pred.shape[1] - 1)
                b = (joint_coords[:, 1] + sigma).clip(max=joint_pred.shape[0] - 1)
            for i, joint_outs in enumerate(zip(joint_coords, l, t, r, b)):
                joint, xmin, ymin, xmax, ymax = joint_outs  # `peak`: (x,y)
                patch = joint_pred[ymin:ymax + 1, xmin:xmax + 1]
                # > gpu
                if not USE_CPU:
                    up_patch = F.interpolate(patch[None, None, :], scale_factor=self.strides[0], mode='bicubic').squeeze()
                    loc_max = torch.tensor((up_patch.argmax() / up_patch.size(1),
                                            up_patch.argmax() % up_patch.size(1))).to(device=device)
                    loc_patch_center = compute_resized_coords(
                        torch.flip(joint, dims=(0,)) - torch.tensor([ymin, xmin]).to(device=device), self.strides[0])
                    joint_score = up_patch[loc_max[0], loc_max[1]]
                    refined_center = loc_max - loc_patch_center
                    scaled_joint = compute_resized_coords(joint, self.strides[0]) + torch.flip(refined_center, dims=(0,))
                    joints.append(SingleJoint(scaled_joint[0], scaled_joint[1], joint_score, total_joint_count))
                else:
                    # > cpu
                    up_patch = mmcv.imrescale(patch, self.strides[0], return_scale=False)
                    loc_max = np.unravel_index(up_patch.argmax(), up_patch.shape)
                    loc_patch_center = compute_resized_coords_cpu(joint[::-1] - [ymin, xmin], self.strides[0])
                    joint_score = up_patch[loc_max]
                    refined_center = loc_max - loc_patch_center
                    scaled_joint = compute_resized_coords_cpu(joint, self.strides[0]) + refined_center[::-1]
                    joints[i, :] = tuple(scaled_joint) + (joint_score, total_joint_count, joint_type)
                total_joint_count += 1
            all_joints.append(joints)
        return all_joints

    def get_bodyparts(self, bodypart_preds, joints_list, img_shape, bodypart_paris):
        connected_limbs = []
        for part_id, bodypart_pair in enumerate(bodypart_paris):
            bodypart_pred = bodypart_preds[part_id, :, :]
            joints_src = joints_list[bodypart_pair[0]]
            joints_dst = joints_list[bodypart_pair[1]]
            if len(joints_src) == 0 and len(joints_dst) == 0:
                connected_limbs.append([])
            else:
                limb_candidates = []
                for src_idx, joint_src in enumerate(joints_src):
                    for dst_idx, joint_dst in enumerate(joints_dst):
                        # limb_dir = torch.FloatTensor([joint_dst.x - joint_src.x, joint_dst.y - joint_src.y])
                        # limb_len = (limb_dir ** 2).sum().sqrt()
                        limb_len = joint_src.distance_to(joint_dst)
                        intermd_steps = min((limb_len + 1).round().int(), self.bodypart_cfg['intermd_points'])
                        if intermd_steps == 0:
                            continue
                        intermd_x = torch.linspace(start=joint_src.x, end=joint_dst.x, steps=intermd_steps).round().long()
                        intermd_y = torch.linspace(start=joint_src.y, end=joint_dst.y, steps=intermd_steps).round().long()
                        intermd_scores = bodypart_pred[intermd_y, intermd_x]

                        limb_score = intermd_scores.mean() + min(0.5 + img_shape[0] / limb_len - 1, 0)
                        criterion1 = (intermd_scores > self.bodypart_cfg['thr']).nonzero().size(0) >= \
                                     intermd_steps * self.bodypart_cfg['min_intermd_factor']
                        criterion2 = limb_score > 0
                        if criterion1 and criterion2:
                            paf_weights = self.bodypart_cfg['weights']
                            overall_score = paf_weights[0] * limb_score + \
                                            paf_weights[1] * joint_src.score + \
                                            paf_weights[2] * joint_dst.score
                            limb_candidates.append([src_idx, dst_idx, limb_len, limb_score, overall_score])
                # greedy algorithm for Bipartite Weighted Graph Matching
                limb_candidates = sorted(limb_candidates, key=lambda x: x[4], reverse=True)
                max_limbs = min(len(joints_src), len(joints_dst))
                limbs = []
                src_cmp_sets = set()
                dst_cmp_sets = set()
                for limb_idx, limb_candidate in enumerate(limb_candidates):
                    src_idx, dst_idx, limb_len, limb_score, overall_score = limb_candidate
                    if src_idx not in src_cmp_sets and dst_idx not in dst_cmp_sets:
                        limbs.append(SingleLimb(
                            joints_src[src_idx].id, joints_dst[dst_idx].id, src_idx, dst_idx, limb_len, limb_score
                        ))
                        src_cmp_sets.add(src_idx)
                        dst_cmp_sets.add(dst_idx)
                        if len(limbs) >= max_limbs:
                            break
                connected_limbs.append(limbs)
        return connected_limbs

    def get_skeletons_cpu(self, paf_preds, joints_list, img_shape):
        joint_preds = np.array(joints_list).reshape(-1, 5)[None, :].astype(np.float32)
        pafprc.process_paf(joint_preds, paf_preds, img_shape[0])
        all_skeletons = []
        for i in range(pafprc.get_num_humans()):
            new_skeleton = SingleSkeleton(self.num_joints, 'cpu')
            is_added = False
            for j in range(self.num_joints):
                joint_id = int(pafprc.get_part_peak_id(i, j))
                if joint_id > -1:
                    is_added = True
                    part_score = pafprc.get_part_score(joint_id)
                    x, y = pafprc.get_part_x(joint_id), pafprc.get_part_y(joint_id)
                    new_skeleton.assign(j, joint_id, x, y, part_score)
            if is_added:
                new_skeleton.score = pafprc.get_score(i)
                all_skeletons.append(new_skeleton)
        return all_skeletons

    def get_skeletons(self, joints_candidates, limbs_list, bodypart_pairs):
        all_skeletons = []
        for limb_type, bodypart_pair in enumerate(bodypart_pairs):
            limbs_candidates = limbs_list[limb_type]
            joint_src_type, joint_dst_type = bodypart_pair
            for limb_idx, limb in enumerate(limbs_candidates):
                skeleton_assoc_idx = []
                for skeleton_idx, skeleton in enumerate(all_skeletons):
                    if skeleton[joint_src_type].id == limb.joint_src_id or \
                       skeleton[joint_dst_type].id == limb.joint_dst_id:
                        skeleton_assoc_idx.append(skeleton_idx)

                if len(skeleton_assoc_idx) == 1:
                    skeleton1 = all_skeletons[skeleton_assoc_idx[0]]
                    if skeleton1[joint_dst_type].id == -1 and \
                       skeleton1.prev_limb_length * self.skeleton_cfg['min_length_ratio'] > limb.length:
                        skeleton1.add(joint_dst_type,
                                      limb.joint_dst_id,
                                      limb,
                                      joints_candidates[limb.joint_dst_id].x,
                                      joints_candidates[limb.joint_dst_id].y,
                                      joints_candidates[limb.joint_dst_id].score)

                    elif (skeleton1[joint_dst_type].id != limb.joint_dst_id and
                          skeleton1[joint_dst_type].score <= limb.score and
                          skeleton1.prev_limb_length * self.skeleton_cfg['min_length_ratio'] > limb.length) or \
                         (skeleton1[joint_dst_type].id == limb.joint_dst_id and
                          skeleton1[joint_dst_type].score <= limb.score):
                        skeleton1.update(joint_dst_type,
                                         limb.joint_dst_id,
                                         limb,
                                         joints_candidates[skeleton1[joint_dst_type].id].score,
                                         joints_candidates[limb.joint_dst_id].x,
                                         joints_candidates[limb.joint_dst_id].y,
                                         joints_candidates[limb.joint_dst_id].score)
                    else:
                        # TODO: update score only when src_id & dst_id are the same.
                        pass
                elif len(skeleton_assoc_idx) == 2:
                    skeleton1_idx, skeleton2_idx = skeleton_assoc_idx[0], skeleton_assoc_idx[1]
                    skeleton1 = all_skeletons[skeleton1_idx]
                    skeleton2 = all_skeletons[skeleton2_idx]
                    merge2one = skeleton1 != skeleton2
                    if merge2one:
                        min_joint_score = skeleton1.cmp(skeleton2, min)
                        if limb.score >= min_joint_score * self.skeleton_cfg['min_score_ratio'] and \
                           limb.length < skeleton1.prev_limb_length * self.skeleton_cfg['min_length_ratio']:
                            skeleton1.merge(skeleton2, limb)
                            del all_skeletons[skeleton2_idx]

                    else:
                        # TODO: compare with scores between skeletons
                        pass

                elif limb_type < len(bodypart_pairs):
                    score = joints_candidates[limb.joint_src_id].score + \
                            joints_candidates[limb.joint_dst_id].score
                    src_x, src_y = joints_candidates[limb.joint_src_id].x, \
                                   joints_candidates[limb.joint_src_id].y
                    dst_x, dst_y = joints_candidates[limb.joint_dst_id].x, \
                                   joints_candidates[limb.joint_dst_id].y
                    new_skeleton = SingleSkeleton(self.num_joints, limb.score.get_device())
                    new_skeleton.add(joint_src_type, limb.joint_src_id, limb, src_x, src_y, score)
                    new_skeleton.add(joint_dst_type, limb.joint_dst_id, limb, dst_x, dst_y, score)
                    all_skeletons.append(new_skeleton)

        skeleton_to_delete = []
        for idx, skeleton in enumerate(all_skeletons):  # > #person
            skeleton.joints = skeleton.joints.cpu().numpy()
            if skeleton.joint_count < 2 or skeleton.score / skeleton.joint_count < 0.35:
                skeleton_to_delete.append(idx)
        all_skeletons = np.delete(all_skeletons, skeleton_to_delete, axis=0)

        return all_skeletons

    def get_points(self, featmap_sizes, dtype, device):
        """Get points according to feature map sizes.

        Args:
            featmap_sizes (list[tuple]): Multi-level feature map sizes.
            dtype (torch.dtype): Type of points.
            device (torch.device): Device of points.

        Returns:
            tuple: points of each image. shape: (w x h, (x,y))
        """
        mlvl_points = []
        for i in range(len(featmap_sizes)):  # > #level of fpn
            mlvl_points.append(
                # TOCHECK: add different scales according to fpn levels
                self.get_points_single(featmap_sizes[i], self.strides[i],
                                       dtype, device))
        return mlvl_points

    def get_points_single(self, featmap_size, stride, dtype, device):
        h, w = featmap_size
        x_range = torch.arange(
            0, w * stride, stride, dtype=dtype, device=device)  # (featW,)
        y_range = torch.arange(
            0, h * stride, stride, dtype=dtype, device=device)  # (featW,)
        y, x = torch.meshgrid(y_range, x_range)  # y,x: (featH, featW)
        # get center points of each grid
        points = torch.stack(
            (x, y), dim=-1) + stride // 2 - 0.5  # # (featH, featW, 2)
        # points = torch.stack(
        #     (x.reshape(-1), y.reshape(-1)), dim=-1) + stride // 2 - 0.5  # (featH * featW, 2)
        point_ranges = torch.stack((x_range, y_range), dim=-1) + stride // 2 - 0.5
        return points, point_ranges

    def simplepose_target(self, all_points, gt_keypoints, gt_masks_ignore, img_metas, bodypart_pairs, featmap_sizes, dtype, device):
        # > init heatmap generation cfg
        variance = 2 * self.gen_cfg['joint_sigma'] * self.gen_cfg['joint_sigma']
        heatmap_size = math.ceil(
            (math.sqrt(-variance * math.log(self.gen_cfg['joint_thr']))) / self.strides[0]
        ) * 2
        offset_size = heatmap_size // 2 + 1  # NOTE: only used in PAFs of OpenPose

        all_targets = []
        erode_kernel = np.ones((3, 3), np.uint8)
        num_img = len(img_metas)
        points, point_ranges = all_points[0]
        featmap_size = featmap_sizes[0]
        for i, keypoints, masks_ignore, img_meta in zip(range(num_img), gt_keypoints, gt_masks_ignore, img_metas):  # > #img
            heatmaps = torch.zeros((self.num_classes, featmap_size[0], featmap_size[1]),
                                   dtype=dtype, device=device)
            self.joint_target(heatmaps, point_ranges, keypoints, heatmap_size)
            self.bodypart_target(heatmaps, points, keypoints, bodypart_pairs)
            # > assign to bg channels in joint head (index: 18, 19)
            masks_ignore = mmcv.imresize(masks_ignore.transpose((1, 2, 0)), featmap_size, interpolation='area')
            mask_all = cv2.erode(masks_ignore[..., 0], kernel=erode_kernel)
            heatmaps[self.num_joints] = torch.from_numpy(mask_all).to(device)
            heatmaps[self.num_joints + 1] = torch.max(heatmaps[:self.num_joints], dim=0).values
            heatmaps = heatmaps.clamp(min=0, max=1)  # (C, H, W)
            all_targets.append(heatmaps)

            # NOTE: `debug` only
            # show the generated ground-truth
            # img = img_meta['img'].data.permute(1, 2, 0).cpu().numpy()
            # img = cv2.resize(img, featmap_size, interpolation=cv2.INTER_NEAREST)
            # plt.imshow(img)
            # limbs = heatmaps.permute(1, 2, 0).cpu().numpy()
            # limbs = np.amax(limbs[..., 20:50], axis=2)
            # plt.imshow(limbs, alpha=1)
            # plt.show()
            # print()
        all_targets = torch.stack(all_targets)
        return all_targets

    def joint_target(self, heatmaps, points, gt_joints, heatmap_size):
        joint_sigma = self.gen_cfg['joint_sigma']
        joint_variance = gt_joints.new_tensor(2 * joint_sigma * joint_sigma)
        joint_mask = gt_joints[..., 2] < 2  # > (#obj, 18, 3)
        pos_gt_joints = gt_joints[joint_mask][:, :2]  # (#pos, 2)
        x_min = (pos_gt_joints[:, 0] / self.strides[0] - heatmap_size // 2).round().int()
        x_max = (pos_gt_joints[:, 0] / self.strides[0] + heatmap_size // 2 + 1).round().int()
        y_min = (pos_gt_joints[:, 1] / self.strides[0] - heatmap_size // 2).round().int()
        y_max = (pos_gt_joints[:, 1] / self.strides[0] + heatmap_size // 2 + 1).round().int()
        joint_minmax = torch.stack([x_min, y_min, x_max, y_max], dim=-1)  # > (#pos, 4)
        positive_mask = torch.all(joint_minmax[:, 2:] >= 0, dim=1)
        joint_minmax = joint_minmax[positive_mask]
        joint_minmax[:, :2] = torch.where(joint_minmax[:, :2] >= 0, joint_minmax[:, :2], joint_minmax.new_tensor(0))
        pos_gt_joints = pos_gt_joints[positive_mask]
        joint_mask_idx = joint_mask.nonzero()[positive_mask]
        for j, mask_idx, minmax in zip(range(joint_minmax.size(0)), joint_mask_idx, joint_minmax):
            _, joint_idx = mask_idx
            x_start, y_start, x_end, y_end = minmax
            exp_x = (-(points[x_start:x_end, 0] - pos_gt_joints[j, 0]) ** 2 / joint_variance).exp()
            exp_y = (-(points[y_start:y_end, 1] - pos_gt_joints[j, 1]) ** 2 / joint_variance).exp()
            exp_out = torch.einsum('i,j->ij', exp_y, exp_x)  # einsum('i,i->ij') == np.outer()
            # > joints: [0, 30), bodyparts: [30, 50)
            heatmaps[joint_idx, y_start:y_end, x_start:x_end] = \
                torch.max(heatmaps[joint_idx, y_start:y_end, x_start:x_end], exp_out)

    def bodypart_target(self, heatmaps, points, gt_joints, bodypart_pairs):
        bodypart_sigma = self.gen_cfg['bodypart_sigma']
        bodypart_min_sigma = self.gen_cfg['bodypart_min_sigma']
        bodypart_thr = self.gen_cfg['bodypart_thr']
        bodypart_start = self.num_joints + self.num_joint_bg
        joint_mask = gt_joints[..., 2] < 2  # (#obj, 18)

        for i, (fr, to) in enumerate(bodypart_pairs):
            v = joint_mask[:, fr] & joint_mask[:, to]  # > (#obj,)
            joint_fr = gt_joints[v, fr, :2]
            joint_to = gt_joints[v, to, :2]
            dnorm = ((joint_to - joint_fr) ** 2).sum(dim=1)
            v_dnorm = dnorm > 0
            min_fr = torch.min(joint_fr, joint_to)
            max_to = torch.max(joint_fr, joint_to)
            min_fr = ((min_fr - bodypart_min_sigma) / self.strides[0]).round().int()
            max_to = ((max_to + bodypart_min_sigma) / self.strides[0]).round().int()
            min_fr = min_fr.clamp(min=0)
            v_max_to = torch.all(max_to >= 0, dim=1)  # TOCHECK: change `>=` to `>`
            v = v_dnorm & v_max_to
            min_fr, max_to = min_fr[v], max_to[v] + 1
            joint_fr, joint_to = joint_fr[v], joint_to[v]
            if min_fr.size(0) > 0:
                part_id = bodypart_start + i
                heatmap_count = torch.zeros(heatmaps.size()[1:], dtype=torch.float32, device=heatmaps.device)
                for p, min_xy, max_xy, fr_xy, to_xy in \
                    zip(range(min_fr.size(0)), min_fr, max_to, joint_fr, joint_to):
                    (min_x, min_y), (max_x, max_y) = min_xy, max_xy
                    x_range = points[min_y:max_y, min_x:max_x, 0]
                    y_range = points[min_y:max_y, min_x:max_x, 1]
                    d = to_xy - fr_xy
                    dx = fr_xy[0] - x_range
                    dy = fr_xy[1] - y_range
                    norm = (d ** 2).sum().sqrt()
                    dist = ((d[0] * dy - d[1] * dx) / (norm + 1e-6)).abs()
                    gaussian_dist = compute_gaussian_distance(bodypart_sigma, dist, 0)
                    gaussian_dist[gaussian_dist <= bodypart_thr] = 0.01
                    gaussian_disk_mask = gaussian_dist > 0
                    heatmaps[part_id, min_y:max_y, min_x:max_x][gaussian_disk_mask] += \
                        gaussian_dist[gaussian_disk_mask]
                    heatmap_count[min_y:max_y, min_x:max_x][gaussian_disk_mask] += 1
                heatmap_count_mask = heatmap_count > 0
                heatmaps[part_id][heatmap_count_mask] /= heatmap_count[heatmap_count_mask]