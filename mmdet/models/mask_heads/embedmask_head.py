import math
import torch
import torch.nn as nn
import numpy as np
import pycocotools.mask as mask_util
from torch.nn import functional as F
from mmdet.core import bbox_overlaps
from mmcv.cnn import normal_init

from mmdet.core import force_fp32, multi_apply
from mmdet.core.mask.utils import bbox2mask
from mmdet.ops import ConvModule
from ..builder import build_loss
from ..registry import HEADS

INF = 1e8


@HEADS.register_module
class EmbedMaskHead(nn.Module):
    def __init__(self,
                 num_classes,
                 in_channels,
                 embed_out_channels,
                 margin_out_channels,
                 feat_channels=256,
                 stacked_convs=4,
                 strides=(4, 8, 16, 32, 64),
                 regress_ranges=((-1, 64), (64, 128), (128, 256), (256, 512),
                                 (512, INF)),
                 box2margin_scale=800,
                 box2margin_block=True,
                 norm_reg_target=False,
                 loss_smooth=dict(type='SmoothLoss', alpha=0.1),
                 loss_mask=dict(),
                 conv_cfg=None,
                 norm_cfg=dict(type='GN', num_groups=32, requires_grad=True),
                 mask_prior_margin=2.0,
                 mask_min_iou=0.5,
                 mask_iou_mode='iou',
                 mask_scale_factor=2,
                 crop_box_padding=1.0):
        super(EmbedMaskHead, self).__init__()

        self.num_classes = num_classes
        self.cls_out_channels = num_classes - 1
        self.in_channels = in_channels
        self.feat_channels = feat_channels
        self.stacked_convs = stacked_convs
        self.strides = strides
        self.regress_ranges = regress_ranges
        self.box2margin_scale = box2margin_scale
        self.box2margin_block = box2margin_block
        self.norm_reg_target = norm_reg_target
        self.embed_out_channels = embed_out_channels
        self.margin_out_channels = margin_out_channels
        self.loss_smooth = build_loss(loss_smooth)
        self.loss_mask = build_loss(loss_mask)
        self.conv_cfg = conv_cfg
        self.norm_cfg = norm_cfg
        self.mask_min_iou = mask_min_iou
        self.mask_iou_mode = mask_iou_mode
        self.mask_scale_factor = mask_scale_factor
        self.crop_box_padding = crop_box_padding
        self.fp16_enabled = False
        self.init_sigma_bias = math.log(-math.log(0.5) / (mask_prior_margin ** 2))
        self._init_layers()

    def _init_layers(self):
        self.mask_convs = nn.ModuleList()
        for i in range(self.stacked_convs):
            chn = self.in_channels if i == 0 else self.feat_channels
            self.mask_convs.append(
                ConvModule(
                    chn,
                    self.feat_channels,
                    3,
                    stride=1,
                    padding=1,
                    conv_cfg=self.conv_cfg,
                    norm_cfg=self.norm_cfg,
                    bias=True if self.norm_reg_target else (self.norm_cfg is None)))
        self.proposal_embed = nn.Conv2d(self.feat_channels, self.embed_out_channels, 3, padding=1)
        self.proposal_margin = nn.Conv2d(4, self.margin_out_channels, 1, padding=0)
        self.pixel_embed = nn.Conv2d(self.feat_channels, self.embed_out_channels, 3, padding=1)
        # self.scales = nn.ModuleList([Scale(1.0) for _ in self.strides])

    def init_weights(self):
        for m in self.mask_convs:
            normal_init(m.conv, std=0.01)
        normal_init(self.proposal_embed, std=0.01)
        normal_init(self.proposal_margin, std=0.01, bias=self.init_sigma_bias)
        normal_init(self.pixel_embed, std=0.01)

    def forward(self, x):
        fpn_feat, reg_out, reg_feat = x
        return multi_apply(self.forward_single, fpn_feat, reg_out, reg_feat, range(0, len(fpn_feat)))

    def forward_single(self, mask_feat, reg_out, reg_feat, level):
        # > proposal embedding
        proposal_embed = self.proposal_embed(reg_feat)

        # > proposal margin
        if self.norm_reg_target:
            margin_x = reg_out * self.strides[level] / self.box2margin_scale
        else:
            margin_x = reg_out / self.box2margin_scale
        if self.box2margin_block:
            margin_x = margin_x.detach()
        margin_pred = self.proposal_margin(margin_x).float().exp()

        # > pixel embedding, extract `first` level of FPN only
        if level == 0:
            for mask_layer in self.mask_convs:
                mask_feat = mask_layer(mask_feat)
            pixel_embed = self.pixel_embed(mask_feat)
        else:
            pixel_embed = None
        return proposal_embed, margin_pred, pixel_embed

    @force_fp32(apply_to=('bbox_preds', 'proposal_embeds', 'proposal_margins', 'pixel_embeds'))
    def loss(self,
             bbox_preds,
             proposal_embeds,
             proposal_margins,
             pixel_embeds,
             gt_bboxes,
             gt_masks,
             gt_labels,
             img_metas,
             cfg,
             gt_bboxes_ignore=None):
        assert len(bbox_preds) == len(proposal_embeds) == len(proposal_margins) == len(pixel_embeds)
        dtype, device = bbox_preds[0].dtype, bbox_preds[0].device
        featmap_sizes = [featmap.size()[-2:] for featmap in bbox_preds]  # > (H, W)
        all_level_points = self.get_points(featmap_sizes, bbox_preds[0].dtype,
                                           bbox_preds[0].device)  # > (HxW, 2) for each level

        num_imgs = len(gt_labels)

        flatten_bbox_preds = torch.cat(
            [pred.view(num_imgs, 4, -1) for pred in bbox_preds], dim=2
            # > MY-TODO: experiment this
            # [pred.view(num_imgs, 4, -1) * self.strides[lvl] for lvl, pred in enumerate(bbox_preds)], dim=2
        ).transpose(1, 2).contiguous()  # > (#imgs, #points, 4)

        flatten_proposal_embeds = torch.cat(
            [pred.view(num_imgs, self.embed_out_channels, -1) for pred in proposal_embeds], dim=2
        ).transpose(1, 2).contiguous()  # > (#imgs, #points, D)

        flatten_proposal_margins = torch.cat(
            [pred.view(num_imgs, self.margin_out_channels, -1) for pred in proposal_margins], dim=2
        ).transpose(1, 2).contiguous()  # > (#imgs, #points, 1)

        gt_masks = [torch.from_numpy(gt_mask).to(device=device) for gt_mask in gt_masks]

        # > `smooth loss`
        embed_preds, margin_preds, embed_means, margin_means = self.embedmask_smooth_target(
            all_level_points, flatten_bbox_preds, flatten_proposal_embeds,
            flatten_proposal_margins, gt_bboxes, gt_masks, img_metas)
        loss_smooth = self.loss_smooth((embed_preds, margin_preds),
                                       (embed_means, margin_means),
                                       avg_factor=num_imgs)
        # > `mask loss`
        # only use first layer of FPN
        pixel_embeds, bbox_targets, pixel_mask_targets = self.embedmask_mask_target(
            pixel_embeds[0], gt_bboxes, featmap_sizes[0], gt_masks)
        all_crop_masks, flatten_mask_probs, flatten_mask_targets = self.get_pixel_embeds(
            pixel_embeds, embed_means, margin_means, bbox_targets, pixel_mask_targets, train=True)

        loss_mask = self.loss_mask(flatten_mask_probs,
                                   flatten_mask_targets,
                                   all_crop_masks,
                                   avg_factor=num_imgs)
        return dict(
            loss_smooth=loss_smooth,
            loss_mask=loss_mask)

    @force_fp32(apply_to=('proposal_embed', 'proposal_margin', 'pixel_embed'))
    def get_masks(self,
                  proposal_embeds,
                  proposal_margins,
                  pixel_embeds,
                  all_topk_inds,
                  img_metas,
                  cfg,
                  rescale=None):
        """
        Parameters:
        -----------
            proposal_embeds: (#levels, (B, 32, H, W))
            proposal_margins:  (#levels, (B, 1, H, W))
            pixel_embeds:  (#levels, (B, 32, H, W))
            img_metas: meta is read by `Collect` in `test_pipeline` of config.
            cfg: read from `test_cfg` in configs
            rescale: value is set in `multi_gpu_test()` in `test.py`
        Returns:
        --------
        """
        assert len(proposal_embeds) == len(proposal_margins)
        num_levels = len(proposal_embeds)
        result_list = []
        for img_id in range(len(img_metas)):
            proposal_embed_pred_list = [
                proposal_embeds[i][img_id].detach() for i in range(num_levels)
            ]
            proposal_margin_pred_list = [
                proposal_margins[i][img_id].detach() for i in range(num_levels)
            ]
            pixel_embed_pred_list = [
                pixel_embeds[0][img_id].unsqueeze(0).detach()
            ]
            img_shape = img_metas[img_id]['img_shape']
            scale_factor = img_metas[img_id]['scale_factor']
            det_masks = self.get_masks_single(proposal_embed_pred_list,
                                              proposal_margin_pred_list,
                                              all_topk_inds, img_shape,
                                              scale_factor, cfg, rescale)
            det_masks = det_masks + tuple(pixel_embed_pred_list)
            result_list.append(det_masks)
        return result_list

    def get_masks_single(self,
                         proposal_embed_preds,
                         proposal_margin_preds,
                         all_topk_inds,
                         img_shape,
                         scale_factor,
                         cfg,
                         rescale=False):
        assert len(proposal_embed_preds) == len(proposal_margin_preds) == len(all_topk_inds)
        mlvl_proposal_embeds = []
        mlvl_proposal_margins = []
        # iterate level by level over feature maps.
        # (HxW) varies from level to level.
        for proposal_embed_pred, proposal_margin_pred, topk_inds in zip(
                proposal_embed_preds, proposal_margin_preds, all_topk_inds):
            # (32, H, W) -> (H, W, 32) -> (HxW, 32)
            proposal_embed_pred = proposal_embed_pred.permute(1, 2, 0).reshape(-1, self.embed_out_channels)
            # (1, H, W) -> (H, W, 1) -> (HxW, 1)
            proposal_margin_pred = proposal_margin_pred.permute(1, 2, 0).reshape(-1)
            if topk_inds is not None:
                proposal_embed_pred = proposal_embed_pred[topk_inds, :]
                proposal_margin_pred = proposal_margin_pred[topk_inds]
            mlvl_proposal_embeds.append(proposal_embed_pred)
            mlvl_proposal_margins.append(proposal_margin_pred)
        # > (H x W x #levels, 32)
        mlvl_proposal_embeds = torch.cat(mlvl_proposal_embeds)
        # > (H x W x #levels)
        mlvl_proposal_margins = torch.cat(mlvl_proposal_margins).unsqueeze(1)
        return mlvl_proposal_embeds, mlvl_proposal_margins

    def get_seg_masks(self, mask_pred, det_labels, test_cfg,
                      img_metas, rescale, scale_factor):
        """Get segmentation masks from mask_pred and bboxes.

        Args:
            mask_pred (Tensor or ndarray): shape (n, #class+1, h, w).
                For single-scale testing, mask_pred is the direct output of
                model, whose type is Tensor, while for multi-scale testing,
                it will be converted to numpy array outside of this method.
            det_labels (Tensor): shape (n, )
            img_shape (Tensor): shape (3, )
            rcnn_test_cfg (dict): rcnn testing config
            ori_shape: original image size

        Returns:
            list[list]: encoded masks
        """
        assert isinstance(mask_pred, torch.Tensor)
        mask_h, mask_w = mask_pred.size()[-2:]
        fpn_stride = self.strides[0] if self.norm_reg_target else 1.0
        stride = fpn_stride / self.mask_scale_factor
        if rescale:
            img_h, img_w = img_metas[0]['ori_shape'][:2]
        else:
            img_h, img_w = img_metas[0]['img_shape'][:2]
            scale_factor = 1.0
        h = np.ceil(mask_h * stride / scale_factor).astype(np.long)
        w = np.ceil(mask_w * stride / scale_factor).astype(np.long)
        masks = F.interpolate(mask_pred.unsqueeze(1).float(),
                              size=(h, w),
                              mode='bilinear',
                              align_corners=False).gt(test_cfg.mask_thr_binary)
        masks = masks[:, :, :img_h, :img_w].cpu().numpy().astype(np.uint8)
        assert isinstance(masks, np.ndarray)

        cls_segms = [[] for _ in range(self.num_classes - 1)]
        labels = det_labels.cpu().numpy() + 1

        for i in range(mask_pred.shape[0]): # > #candidates
            label = labels[i]
            mask_pred_ = masks[i, ...].transpose(1, 2, 0)
            rle = mask_util.encode(np.array(mask_pred_, dtype=np.uint8, order='F'))[0]
            cls_segms[label - 1].append(rle)

        return cls_segms

    def get_pixel_embeds(self, pixel_embeds, embed_means, margin_means, bbox_targets,
                         pixel_mask_targets=None, train=False):
        flatten_mask_probs = []
        flatten_mask_targets = []
        all_crop_masks = []
        num_imgs = len(bbox_targets)
        for img_id, pixel_embed, embed_mean, margin_mean, bbox_target in \
                zip(range(num_imgs), pixel_embeds, embed_means, margin_means, bbox_targets): # > #imgs
            # > compute mask probabilities
            num_objs = bbox_target.size()[0]
            # > training
            if train:
                crop_masks = []
                crop_mask_probs = []
                for obj_id in range(num_objs):
                    if len(embed_mean[obj_id]) > 0:
                        mask_prob = self.embedmask_mask_prob_single(pixel_embed,
                                                                    # > train
                                                                    embed_mean[obj_id],
                                                                    margin_mean[obj_id],
                                                                    # > test
                                                                    # embed_mean[None, obj_id],
                                                                    # margin_mean[None, obj_id],
                                                                    1)
                        crop_mask = bbox2mask(bbox_target[None, obj_id],
                                              mask_prob.size(), mask_prob.device,
                                              self.crop_box_padding, multi_dim=False)
                        crop_mask_prob = mask_prob * crop_mask.float()
                        crop_masks.append(crop_mask.squeeze())
                        crop_mask_probs.append(crop_mask_prob.squeeze())
                    else:
                        crop_masks.append(bbox_target.new_tensor([]))
                        crop_mask_probs.append(bbox_target.new_tensor([]))
                all_crop_masks.append(crop_masks)
                flatten_mask_probs.append(crop_mask_probs)
            else:
                # > testing
                mask_prob = self.embedmask_mask_prob_single(pixel_embed, # > (32, H, W)
                                                            embed_mean, # > (#objs, 32)
                                                            margin_mean, # > (#objs, 1)
                                                            num_objs)
                # > crop masks by bboxes
                crop_mask = bbox2mask(bbox_target, mask_prob.size(),
                                      mask_prob.device, self.crop_box_padding, multi_dim=True)
                crop_mask_prob = mask_prob * crop_mask.float()
                all_crop_masks.append(crop_mask)
                flatten_mask_probs.append(crop_mask_prob)
            if pixel_mask_targets is not None:
                mask_target = pixel_mask_targets[img_id]
                flatten_mask_targets.append(mask_target.float())

        if pixel_mask_targets is not None:
            return all_crop_masks, flatten_mask_probs, flatten_mask_targets

        return all_crop_masks, flatten_mask_probs

    def get_points(self, featmap_sizes, dtype, device):
        """Get points according to feature map sizes.

        Args:
            featmap_sizes (list[tuple]): Multi-level feature map sizes.
            dtype (torch.dtype): Type of points.
            device (torch.device): Device of points.

        Returns:
            tuple: points of each image. shape: (w x h, (x,y))
        """
        mlvl_points = []
        for i in range(len(featmap_sizes)):
            mlvl_points.append(
                self.get_points_single(featmap_sizes[i], self.strides[i],
                                       dtype, device))
        return mlvl_points

    def get_points_single(self, featmap_size, stride, dtype, device):
        h, w = featmap_size
        x_range = torch.arange(
            0, w * stride, stride, dtype=dtype, device=device)
        y_range = torch.arange(
            0, h * stride, stride, dtype=dtype, device=device)
        y, x = torch.meshgrid(y_range, x_range)
        # get center points of each grid
        points = torch.stack(
            (x.reshape(-1), y.reshape(-1)), dim=-1) + stride // 2
        return points

    def embedmask_smooth_target(self, points, flatten_bbox_preds, flatten_proposal_embeds, flatten_proposal_margins,
                                gt_bboxes_list, gt_masks_list, img_metas):
        assert len(points) == len(self.regress_ranges)

        # concat all levels points and regress ranges
        concat_points = torch.cat(points, dim=0)  # (H x W, 2) x #levels -> (H x W x #levels, 2)
        # get labels and bbox_targets of each image
        proposal_embed_targets, proposal_margin_targets, embed_means, margin_means = multi_apply(
            self.embedmask_smooth_target_single,
            flatten_bbox_preds,  # > (#imgs, #points, 4)
            flatten_proposal_embeds,  # > (#imgs, #points, D)
            flatten_proposal_margins,  # > (#imgs, #points, 1)
            gt_bboxes_list,  # > (#imgs, #objs, 4)
            gt_masks_list,  # > (#imgs, #objs, H, W)
            img_metas,  # (#imgs,)
            points=concat_points,  # > (#points, (x, y))
        )

        return proposal_embed_targets, proposal_margin_targets, embed_means, margin_means

    def embedmask_smooth_target_single(self, flatten_bbox_preds, flatten_proposal_embeds, flatten_proposal_margins,
                                       gt_bboxes, gt_masks, img_meta, points):
        assert 'bbox_targets' in img_meta
        matched_inds = img_meta['bbox_targets']['matched_inds']
        empty_tensor = flatten_proposal_embeds.new_tensor([])
        num_objs = gt_masks.size(0)  # > (#objs)

        # > get positive proposal indices
        proposal_embed_targets = []
        proposal_margin_targets = []
        for obj_id in range(num_objs):  # MY-TODO: vectorize by `NMS`
            valid = matched_inds == obj_id
            if valid.sum() == 0:
                proposal_embed_targets.append(empty_tensor)
                proposal_margin_targets.append(empty_tensor)
                continue
            pos_points = points[valid]
            pos_bbox_preds = flatten_bbox_preds[valid]
            pos_bboxes = torch.stack([
                pos_points[:, 0] - pos_bbox_preds[:, 0],
                pos_points[:, 1] - pos_bbox_preds[:, 1],
                pos_points[:, 0] + pos_bbox_preds[:, 2],
                pos_points[:, 1] + pos_bbox_preds[:, 3],
            ], dim=1)  # > (#pos, 4)
            target_bbox = gt_bboxes[None, obj_id, :]
            ious = bbox_overlaps(pos_bboxes, target_bbox, mode=self.mask_iou_mode)  # > (#pos, 1)
            iou_in_target = ious[:, 0]  # > (#pos, 1) -> (#pos)
            pos_labels = torch.zeros_like(valid)  # (#points)
            if iou_in_target.max() > self.mask_min_iou:
                pos_in_target = iou_in_target > self.mask_min_iou
            else:  # > at least select one candidate proposal
                pos_in_target = iou_in_target == iou_in_target.max()
            pos_labels[valid] = pos_in_target
            # > each instance may contain multiple positive indices.
            pos_label_inds = pos_labels.nonzero().squeeze(1)
            pos_proposal_embeds = flatten_proposal_embeds[pos_label_inds]
            pos_proposal_margins = flatten_proposal_margins[pos_label_inds]
            proposal_embed_targets.append(pos_proposal_embeds)
            proposal_margin_targets.append(pos_proposal_margins)
        embed_means = [targets.mean(dim=0).unsqueeze(0) if len(targets) > 0 else empty_tensor
                       for targets in proposal_embed_targets]
        margin_means = [targets.mean(dim=0).unsqueeze(0) if len(targets) > 0 else empty_tensor
                        for targets in proposal_margin_targets]

        return proposal_embed_targets, proposal_margin_targets, embed_means, margin_means

    def embedmask_mask_target(self, pixel_embeds, bboxes, featmap_size, masks=None,
                              rescale=False, scale_factor=1.0):
        scale_fn = lambda x, y: [int(i * y) for i in x]
        mask_size = tuple(featmap_size)
        up_size = tuple(scale_fn(mask_size, self.strides[0]))
        down_size = tuple(scale_fn(mask_size, self.mask_scale_factor))
        stride = (self.strides[0] if self.norm_reg_target else 1.0) / self.mask_scale_factor

        # feat_size -> feat_size x scale_factor
        if mask_size != down_size:  # MY-TODO: check this in test pipeline
            pixel_embeds = F.interpolate(input=pixel_embeds,
                                         size=down_size, mode='bilinear', align_corners=False)
        if masks is not None:
            # -> (#imgs, #objs, 4)
            bbox_targets = [bbox / stride for bbox in bboxes]
            # -> (#imgs, #objs, down_H, down_W)
            pixel_mask_targets = []
            for mask in masks:
                pixel_mask_targets.append(self.embedmask_mask_target_single(
                    mask, up_size=up_size, down_size=down_size))

            return pixel_embeds, bbox_targets, pixel_mask_targets

        assert bboxes.size()[-1] == 5
        bbox_targets = bboxes[:, :4].clone().detach()
        if rescale:
            bbox_targets = bbox_targets * scale_factor
        bbox_targets /= stride
        return pixel_embeds, bbox_targets

    def embedmask_mask_target_single(self, gt_masks, up_size, down_size, run_on_cpu=True):
        num_objs, H, W = gt_masks.size()
        if run_on_cpu:
            gpu_device = gt_masks.device
            gt_masks = gt_masks.detach().cpu()
        # > get down-sampled pixel mask
        pixel_mask_target = gt_masks.new_zeros((num_objs, up_size[0], up_size[1]))
        pixel_mask_target[:, :H, :W] = gt_masks
        pixel_mask_target = F.interpolate(
            # > `bilinear` mode needs `4D` input
            input=pixel_mask_target.float().unsqueeze(0),
            size=down_size, mode='bilinear', align_corners=False)[0].gt(0)
        if run_on_cpu:
            pixel_mask_target = pixel_mask_target.to(gpu_device)
        return pixel_mask_target

    def embedmask_mask_prob_single(self, mask_preds, means, margins, num_objs):
        assert len(means) == len(margins)
        feat_H, feat_W = mask_preds.size()[-2:]  # > (#objs, H, W)

        # > (32, H, W) -> (#objs, H, W, 32)
        mask_preds = mask_preds.permute(1, 2, 0).unsqueeze(0).expand(num_objs, -1, -1, -1)
        means = means.view(num_objs, 1, 1, -1).expand(-1, feat_H, feat_W, -1)  # > (#objs, H, W, 32)
        margins = margins.view(num_objs, 1, 1).expand(-1, feat_H, feat_W)  # > (#objs, H, W)
        # > See paper `3.3 Learnable Margin`
        vars = torch.sum((mask_preds - means) ** 2, dim=3)  # > (#objs, H, W)
        prob = torch.exp(-vars * margins)
        return prob
