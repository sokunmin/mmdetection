import torch.nn as nn

pool_cfg = {
    'MaxPool': nn.MaxPool2d,
    'AvgPool': nn.AvgPool2d,
    'AdpAvgPool': nn.AdaptiveAvgPool2d
}


def build_pool_layer(cfg, *args, **kwargs):
    """ Build pooliong layer

    Args:
        cfg (dict): cfg should contain:
            type (str): identify norm layer type.
            layer args: args needed to instantiate a norm layer.
            requires_grad (bool): [optional] whether stop gradient updates
        num_features (int): number of channels from input.
        postfix (int, str): appended into norm abbreviation to
            create named layer.

    Returns:
        name (str): abbreviation + postfix
        layer (nn.Module): created norm layer
    """
    if cfg is None:
        cfg_ = dict(type='AvgPool')
    else:
        assert isinstance(cfg, dict) and 'type' in cfg
        cfg_ = cfg.copy()

    layer_type = cfg_.pop('type')
    if layer_type not in pool_cfg:
        raise KeyError('Unrecognized pooling type {}'.format(layer_type))
    else:
        pool_layer = pool_cfg[layer_type]

    layer = pool_layer(*args, **kwargs, **cfg_)

    return layer