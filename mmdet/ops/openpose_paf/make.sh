#!/bin/sh
if [ -f "pafprocess.py" ]; then
  rm pafprocess.py
fi
if [ -f "pafprocess_wrap.cpp" ]; then
  rm pafprocess_wrap.cpp
fi
if [ -f "pafprocess_wrap.cxx" ]; then
  rm pafprocess_wrap.cxx
fi
if [ -f "_pafprocess.cpython-37m-x86_64-linux-gnu.so" ]; then
  rm _pafprocess.cpython-37m-x86_64-linux-gnu.so
fi
if [ -d "build" ]; then
  rm -rf build/
fi
swig -python -c++ pafprocess.i
python3 setup.py build_ext --inplace