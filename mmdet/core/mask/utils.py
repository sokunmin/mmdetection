import mmcv
import torch

def split_combined_polys(polys, poly_lens, polys_per_mask):
    """Split the combined 1-D polys into masks.

    A mask is represented as a list of polys, and a poly is represented as
    a 1-D array. In dataset, all masks are concatenated into a single 1-D
    tensor. Here we need to split the tensor into original representations.

    Args:
        polys (list): a list (length = image num) of 1-D tensors
        poly_lens (list): a list (length = image num) of poly length
        polys_per_mask (list): a list (length = image num) of poly number
            of each mask

    Returns:
        list: a list (length = image num) of list (length = mask num) of
            list (length = poly num) of numpy array
    """
    mask_polys_list = []
    for img_id in range(len(polys)):
        polys_single = polys[img_id]
        polys_lens_single = poly_lens[img_id].tolist()
        polys_per_mask_single = polys_per_mask[img_id].tolist()

        split_polys = mmcv.slice_list(polys_single, polys_lens_single)
        mask_polys = mmcv.slice_list(split_polys, polys_per_mask_single)
        mask_polys_list.append(mask_polys)
    return mask_polys_list


def bbox2mask(box, size, device, padding=0.0, multi_dim=False):
    num_objs, h, w = size

    b_w = box[..., 2] - box[..., 0]
    b_h = box[..., 3] - box[..., 1]
    x1 = torch.clamp(box[..., 0] - b_w * padding - 1, min=0).unsqueeze(1)
    x2 = torch.clamp(box[..., 2] + b_w * padding + 1, max=w-1).unsqueeze(1)
    y1 = torch.clamp(box[..., 1] - b_h * padding - 1, min=0).unsqueeze(1)
    y2 = torch.clamp(box[..., 3] + b_h * padding + 1, max=h-1).unsqueeze(1)

    rows = torch.arange(w, device=device, dtype=x1.dtype).view(1, 1, -1).expand(num_objs, h, w)
    cols = torch.arange(h, device=device, dtype=x1.dtype).view(1, -1, 1).expand(num_objs, h, w)

    if multi_dim:
        x1 = x1[..., None]
        x2 = x2[..., None]
        y1 = y1[..., None]
        y2 = y2[..., None]

    masks_left = rows >= x1.expand(num_objs, 1, 1)
    masks_right = rows < x2.expand(num_objs, 1, 1)
    masks_up = cols >= y1.expand(num_objs, 1, 1)
    masks_down = cols < y2.expand(num_objs, 1, 1)

    crop_mask = masks_left * masks_right * masks_up * masks_down
    return crop_mask