import mmcv


def coco_keypoints():
    return [
        'nose', 'left eye', 'right eye', 'left ear', 'right ear',
        'left shoulder', 'right shoulder', 'left elbow', 'right elbow',
        'left wrist', 'right wrist', 'left hip', 'right hip',
        'left knee', 'right knee', 'left ankle', 'right ankle'
    ]


def cmu_keypoints():
    return [
        'nose', 'neck', 'right shoulder', 'right elbow', 'right wrist',
        'left shoulder', 'left elbow', 'left wrist', 'right hip',
        'right knee', 'right ankle', 'left hip', 'left knee', 'left ankle',
        'right eye', 'left eye', 'right ear', 'left ear'
    ]


def coco_bodyparts():
    return [
        (0, 1), (0, 2), (1, 3), (2, 4), (3, 5), (4, 6), (5, 6),
        (5, 7), (7, 9), (6, 8), (8, 10), (5, 11), (6, 12), (11, 12),
        (11, 13), (12, 14), (13, 15), (14, 16)
    ]


def cmu_bodyparts():
    return [
        (14, 16), (12, 14), (15, 13), (11, 13), (12, 11), (6, 12),
        (5, 11), (8, 10), (6, 8), (7, 9), (5, 7), (17, 5),
        (17, 6), (0, 1), (0, 2), (2, 4), (1, 3), (0, 17)
    ]


skeleton_aliases = {
    'coco': ['coco'],
    'cmu': ['cmu']
}


def get_skeletons(dataset):
    """Get keypoint names of a dataset."""
    alias2name = {}
    for name, aliases in skeleton_aliases.items():
        for alias in aliases:
            alias2name[alias] = name

    if mmcv.is_str(dataset):
        if dataset in alias2name:
            keypoints = eval(alias2name[dataset] + '_keypoints()')
            bodyparts = eval(alias2name[dataset] + '_bodyparts()')
        else:
            raise ValueError('Unrecognized dataset: {}'.format(dataset))
    else:
        raise TypeError('dataset must a str, but got {}'.format(type(dataset)))
    return keypoints, bodyparts

