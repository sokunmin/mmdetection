import math
import torch


def bbox_overlaps(bboxes1, bboxes2, mode='iou', is_aligned=False):
    """Calculate overlap between two set of bboxes.

    If ``is_aligned`` is ``False``, then calculate the ious between each bbox
    of bboxes1 and bboxes2, otherwise the ious between each aligned pair of
    bboxes1 and bboxes2.

    Args:
        bboxes1 (Tensor): shape (m, 4) in <x1, y1, x2, y2> format.
        bboxes2 (Tensor): shape (n, 4) in <x1, y1, x2, y2> format.
            If is_aligned is ``True``, then m and n must be equal.
        mode (str): "iou" (intersection over union) or iof (intersection over
            foreground).

    Returns:
        ious(Tensor): shape (m, n) if is_aligned == False else shape (m, 1)

    Example:
        >>> bboxes1 = torch.FloatTensor([
        >>>     [0, 0, 10, 10],
        >>>     [10, 10, 20, 20],
        >>>     [32, 32, 38, 42],
        >>> ])
        >>> bboxes2 = torch.FloatTensor([
        >>>     [0, 0, 10, 20],
        >>>     [0, 10, 10, 19],
        >>>     [10, 10, 20, 20],
        >>> ])
        >>> bbox_overlaps(bboxes1, bboxes2)
        tensor([[0.5238, 0.0500, 0.0041],
                [0.0323, 0.0452, 1.0000],
                [0.0000, 0.0000, 0.0000]])

    Example:
        >>> empty = torch.FloatTensor([])
        >>> nonempty = torch.FloatTensor([
        >>>     [0, 0, 10, 9],
        >>> ])
        >>> assert tuple(bbox_overlaps(empty, nonempty).shape) == (0, 1)
        >>> assert tuple(bbox_overlaps(nonempty, empty).shape) == (1, 0)
        >>> assert tuple(bbox_overlaps(empty, empty).shape) == (0, 0)
    """

    assert mode in ['iou', 'iof', 'giou', 'diou', 'ciou']

    rows = bboxes1.size(0)
    cols = bboxes2.size(0)
    if is_aligned:
        assert rows == cols

    if rows * cols == 0:
        return bboxes1.new(rows, 1) if is_aligned else bboxes1.new(rows, cols)

    if is_aligned:
        lt = torch.max(bboxes1[:, :2], bboxes2[:, :2])  # [rows, 2]
        rb = torch.min(bboxes1[:, 2:], bboxes2[:, 2:])  # [rows, 2]
        overlap_wh = (rb - lt + 1).clamp(min=0)
        overlap = overlap_wh[:, 0] * overlap_wh[:, 1]
        w1 = bboxes1[:, 2] - bboxes1[:, 0]
        h1 = bboxes1[:, 3] - bboxes1[:, 1]
        area1 = (w1 + 1) * (h1 + 1)

        if mode != 'iof':
            w2 = bboxes2[:, 2] - bboxes2[:, 0]
            h2 = bboxes2[:, 3] - bboxes2[:, 1]
            area2 = (w2 + 1) * (h2 + 1)
            union = (area1 + area2 - overlap)
            ious = overlap / union
            if mode == 'iou':
                return ious

            # > giou: enclosing shape
            lt = torch.min(bboxes1[:, :2], bboxes2[:, :2])  # [rows, 2]
            rb = torch.max(bboxes1[:, 2:], bboxes2[:, 2:])  # [rows, 2]
            closure_wh = (rb - lt + 1).clamp(min=0)
            closure = closure_wh[:, 0] * closure_wh[:, 1]
            gious = ious - (closure - union) / closure
            if mode == 'giou':
                return gious

            # > diou: center point & diagonal distance
            ctr_b1 = (bboxes1[:, 2:] + bboxes1[:, :2]) / 2  # [rows, (x,y)]
            ctr_b2 = (bboxes2[:, 2:] + bboxes2[:, :2]) / 2  # [rows, (x,y)]

            ctr_diag = torch.sum((ctr_b2 - ctr_b1) ** 2, dim=1)
            closure_diag = (closure_wh[:, 0] ** 2) + (closure_wh[:, 1] ** 2)
            u = ctr_diag / closure_diag
            dious = ious - u
            if mode == 'diou':
                return dious

            # > ciou: trade-off param & consistency of aspect ratio
            with torch.no_grad():
                arctan = torch.atan(w2 / h2) - torch.atan(w1 / h1)
                v = (4 / (math.pi ** 2)) * torch.pow(arctan, 2)
                alpha = v / ((1 - ious) + v)
                w_temp = 2 * w1
            ar = - (8 / (math.pi ** 2)) * arctan * ((w1 - w_temp) * h1)
            cious = ious - (u + alpha * ar)
            if mode == 'ciou':
                return cious
        else:
            ious = overlap / area1
            return ious
    else:
        lt = torch.max(bboxes1[:, None, :2], bboxes2[:, :2])  # [rows, cols, 2]
        rb = torch.min(bboxes1[:, None, 2:], bboxes2[:, 2:])  # [rows, cols, 2]
        overlap_wh = (rb - lt + 1).clamp(min=0)  # [rows, cols, 2]
        overlap = overlap_wh[:, :, 0] * overlap_wh[:, :, 1]
        w1 = bboxes1[:, 2] - bboxes1[:, 0]
        h1 = bboxes1[:, 3] - bboxes1[:, 1]
        area1 = (w1 + 1) * (h1 + 1)

        if mode != 'iof':
            w2 = bboxes2[:, 2] - bboxes2[:, 0]
            h2 = bboxes2[:, 3] - bboxes2[:, 1]
            area2 = (w2 + 1) * (h2 + 1)
            union = (area1[:, None] + area2 - overlap)
            ious = overlap / union  # [rows, cols]
            if mode == 'iou':
                return ious

            # > giou: enclosing shape
            lt = torch.min(bboxes1[:, None, :2], bboxes2[:, :2])  # [rows, cols, 2]
            rb = torch.max(bboxes1[:, None, 2:], bboxes2[:, 2:])  # [rows, cols, 2]
            closure_wh = (rb - lt + 1).clamp(min=0)  # [rows, cols, 2]
            closure = closure_wh[:, :, 0] * closure_wh[:, :, 1]
            gious = ious - (closure - union) / closure  # [rows, cols]
            if mode == 'giou':
                return gious

            # > diou: center point & diagonal distance
            ctr_b1 = (bboxes1[:, None, 2:] + bboxes1[:, None, :2]) / 2  # [rows, cols, 2]
            ctr_b2 = (bboxes2[:, 2:] + bboxes2[:, :2]) / 2  # [rows, 2]
            ctr_diag = torch.sum((ctr_b2 - ctr_b1) ** 2, dim=2)  # [rows, 2]
            closure_diag = (closure_wh[:, :, 0] ** 2) + (closure_wh[:, :, 1] ** 2)  # [rows, 2]
            u = ctr_diag / closure_diag  # [rows, 2]
            dious = ious - u  # [rows, cols]
            if mode == 'diou':
                return dious

            # > ciou: trade-off param & consistency of aspect ratio
            with torch.no_grad():
                arctan = torch.atan(w2 / h2) - torch.atan(w1[:, None] / h1[:, None])  # [rows, 2]
                v = (4 / (math.pi ** 2)) * torch.pow(arctan, 2)
                alpha = v / ((1 - ious) + v)
                w_temp = 2 * w1
            ar = - (8 / (math.pi ** 2)) * arctan * ((w1[:, None] - w_temp[:, None]) * h1[:, None])
            cious = ious - (u + alpha * ar)  # [rows, cols]
            if mode == 'ciou':
                return cious
        else:
            ious = overlap / (area1[:, None])
            return ious