import torch
import numpy as np
from mmdet.ops.nms import nms_wrapper
from scipy.ndimage.morphology import generate_binary_structure
from scipy.ndimage.filters import maximum_filter


def multiclass_nms(multi_bboxes,
                   multi_scores,
                   score_thr,
                   nms_cfg,
                   max_num=-1,
                   score_factors=None):
    """NMS for multi-class bboxes.

    Args:
        multi_bboxes (Tensor): shape (n, #class*4) or (n, 4)
        multi_scores (Tensor): shape (n, #class), where the 0th column
            contains scores of the background class, but this will be ignored.
        score_thr (float): bbox threshold, bboxes with scores lower than it
            will not be considered.
        nms_thr (float): NMS IoU threshold
        max_num (int): if there are more than max_num bboxes after NMS,
            only top max_num will be kept.
        score_factors (Tensor): (#cands,), The factors multiplied to scores before
            applying NMS

    Returns:
        tuple: (bboxes, labels), tensors of shape (k, 5) and (k, 1). Labels
            are 0-based.
    """
    num_classes = multi_scores.size(1) - 1
    # exclude background category
    if multi_bboxes.shape[1] > 4:
        bboxes = multi_bboxes.view(multi_scores.size(0), -1, 4)[:, 1:]
    else:
        bboxes = multi_bboxes[:, None].expand(-1, num_classes, 4)  # (#cands, 4) -> (#cands, #cls, 4)
    scores = multi_scores[:, 1:]  # (#cands, #cls)

    # filter out boxes with low scores
    valid_mask = scores > score_thr  # (#pos, #cls)
    bboxes = bboxes[valid_mask]  # (#pos, 4)
    if score_factors is not None:
        scores = scores * score_factors[:, None]
    scores = scores[valid_mask]  # (#cands, #cls) -> (#pos,)
    labels = valid_mask.nonzero()[:, 1]  # (#pos, 2) -> (#pos,)

    if bboxes.numel() == 0:
        bboxes = multi_bboxes.new_zeros((0, 5))
        labels = multi_bboxes.new_zeros((0, ), dtype=torch.long)
        return bboxes, labels

    # Modified from https://github.com/pytorch/vision/blob
    # /505cd6957711af790211896d32b40291bea1bc21/torchvision/ops/boxes.py#L39.
    # strategy: in order to perform NMS independently per class.
    # we add an offset to all the boxes. The offset is dependent
    # only on the class idx, and is large enough so that boxes
    # from different classes do not overlap
    max_coordinate = bboxes.max()  # (#pos, 4) -> scalar
    offsets = labels.to(bboxes) * (max_coordinate + 1)  # (#pos,)
    bboxes_for_nms = bboxes + offsets[:, None]  # (#pos, 4)
    nms_cfg_ = nms_cfg.copy()
    nms_type = nms_cfg_.pop('type', 'nms')
    nms_op = getattr(nms_wrapper, nms_type)
    dets, keep = nms_op(
        torch.cat([bboxes_for_nms, scores[:, None]], 1), **nms_cfg_)  # (#pos, 5) -> (#nms_pos, 5), (#nms_pos,)
    bboxes = bboxes[keep]  # (#nms_pos, 4)
    scores = dets[:, -1]  # (#nms_pos,), soft_nms will modify scores
    labels = labels[keep]  # (#nms_pos,)

    if keep.size(0) > max_num:
        _, inds = scores.sort(descending=True)
        inds = inds[:max_num]
        bboxes = bboxes[inds]
        scores = scores[inds]
        labels = labels[inds]
    # -> (#max, 5), (#max,)
    return torch.cat([bboxes, scores[:, None]], 1), labels


# for CMU
def cmu_heatmap_nms(multi_heatmaps, score_thr):
    valid_mask = multi_heatmaps > score_thr  # (#pos, #cls)
    max_heatmaps = maximum_filter(multi_heatmaps.detach().cpu().numpy(), footprint=generate_binary_structure(2, 1))
    max_heatmaps = torch.from_numpy(max_heatmaps).to(multi_heatmaps.get_device())
    heatmaps = multi_heatmaps == max_heatmaps
    heatmaps = heatmaps * valid_mask
    heatmaps = torch.flip(heatmaps.nonzero(), dims=(1,))
    return heatmaps  # > (#pos,(y,x)) -> (#pos, (x,y))


def cmu_heatmap_nms_cpu(multi_heatmaps, score_thr):
    valid_mask = multi_heatmaps > score_thr  # (#pos, #cls)
    max_heatmaps = maximum_filter(multi_heatmaps, footprint=generate_binary_structure(2, 1))
    peaks_binary = (max_heatmaps == multi_heatmaps) * valid_mask
    return np.array(np.nonzero(peaks_binary)[::-1]).T


# My-TODO: refactor this to newer version.
def multitask_nms(multi_bboxes,
                  multi_scores,
                  multi_tasks,
                  score_thr,
                  nms_cfg,
                  max_num=-1,
                  score_factors=None,
                  extra_cfg=None):
    """NMS for multi-class bboxes.

    Args:
        multi_bboxes (Tensor): shape (n, #class*4) or (n, 4)
        multi_scores (Tensor): shape (n, #class), where the 0th column
            contains scores of the background class, but this will be ignored.
        multi_tasks (Dict): shape (task_name, (n, #class * dim))
        score_thr (float): bbox threshold, bboxes with scores lower than it
            will not be considered.
        nms_thr (float): NMS IoU threshold
        max_num (int): if there are more than max_num bboxes after NMS,
            only top max_num will be kept.
        score_factors (Tensor): The factors multiplied to scores before
            applying NMS

    Returns:
        tuple: (bboxes, labels), tensors of shape (k, 5) and (k, 1). Labels
            are 0-based.
    """
    num_classes = multi_scores.shape[1]
    bboxes, labels = [], []
    tasks = {k:[] for k in multi_tasks.keys()}
    nms_cfg_ = nms_cfg.copy()
    nms_type = nms_cfg_.pop('type', 'nms')
    nms_op = getattr(nms_wrapper, nms_type)
    for i in range(1, num_classes):
        cls_inds = multi_scores[:, i] > score_thr
        if not cls_inds.any():
            continue
        # get bboxes and scores of this class
        _tasks = {}
        if multi_bboxes.shape[1] == 4:
            _bboxes = multi_bboxes[cls_inds, :]
            for k, v in multi_tasks.items():
                assert len(v.size()) == 2
                _tasks[k] = v[cls_inds, :]
        else:
            _bboxes = multi_bboxes[cls_inds, i * 4:(i + 1) * 4]
        _scores = multi_scores[cls_inds, i]
        if score_factors is not None:
            _scores *= score_factors[cls_inds]
        # (n, 4) + (n, 1) -> (n, 5), [n, (bbox, score)]
        cls_dets = torch.cat([_bboxes, _scores[:, None]], dim=1)
        cls_dets, nms_inds = nms_op(cls_dets, **nms_cfg_)
        cls_labels = multi_bboxes.new_full((cls_dets.shape[0], ),
                                           i - 1,
                                           dtype=torch.long)
        bboxes.append(cls_dets)
        labels.append(cls_labels)
        if bool(_tasks):
            for k, v in _tasks.items():
                tasks[k].append(v[nms_inds])
    if bboxes:
        bboxes = torch.cat(bboxes)
        labels = torch.cat(labels)
        if bool(tasks):
            for k, v in tasks.items():
                tasks[k] = torch.cat(v)
        if bboxes.shape[0] > max_num:
            _, inds = bboxes[:, -1].sort(descending=True)
            inds = inds[:max_num]
            bboxes = bboxes[inds]
            labels = labels[inds]
            if bool(tasks):
                for k, v in tasks.items():
                    tasks[k] = v[inds]
    else:
        bboxes = multi_bboxes.new_zeros((0, 5))
        labels = multi_bboxes.new_zeros((0, ), dtype=torch.long)
        for k, v in tasks.items():
            tasks[k] = multi_bboxes.new_zeros((0, extra_cfg[k]))
    return bboxes, labels, tasks