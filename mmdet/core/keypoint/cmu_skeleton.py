import torch
import numpy as np


class SingleJoint(object):

    __slots__ = ('x', 'y', 'score', 'id')

    def __init__(self, x, y, score, id):
        self.x = x
        self.y = y
        self.score = score
        self.id = id

    def distance_to(self, other):
        assert isinstance(other, SingleJoint)
        dir = torch.FloatTensor([other.x - self.x, other.y - self.y])
        return (dir ** 2).sum().sqrt()


class ConnectedJoint(object):

    __slots__ = ('id', 'score')

    def __init__(self, id, score):
        self.id = id
        self.score = score

    def connect(self, id, score):
        self.id = id
        self.score = score


class SingleLimb(object):

    __slots__ = ('joint_src_id', 'joint_dst_id', 'src_idx', 'dst_idx', 'length', 'score')

    def __init__(self, joint_src_id, joint_dst_id, src_idx, dst_idx, length, score):
        self.joint_src_id = joint_src_id
        self.joint_dst_id = joint_dst_id
        self.src_idx = src_idx
        self.dst_idx = dst_idx
        self.length = length
        self.score = score


class SingleSkeleton(object):

    __slots__ = ('num_keypoints', 'device', 'joints', 'joint_ids', 'joint_scores', 'score', 'prev_limb_length', 'joint_count')

    def __init__(self, num_keypoints, device):
        self.num_keypoints = num_keypoints
        self.device = device
        if device == 'cpu':
            self.joint_ids = -1 * np.ones(num_keypoints, dtype=np.int32)
            self.joint_scores = -1. * np.ones(num_keypoints, dtype=np.float32)
            self.joints = np.zeros((num_keypoints, 3), dtype=np.float64)
        else:
            self.joint_ids = -1 * torch.ones(num_keypoints, dtype=torch.int32, device=device)
            self.joint_scores = -1. * torch.ones(num_keypoints, dtype=torch.float32, device=device)
            self.joints = torch.zeros((num_keypoints, 3), dtype=torch.float64, device=device)
        self.score = -1.
        self.prev_limb_length = -1.
        self.joint_count = 0

    def __getitem__(self, idx):
        if self.device == 'cpu':
            return self.joints[3 * idx:3 * idx + 3]
        else:
            return ConnectedJoint(self.joint_ids[idx], self.joint_scores[idx])

    def __ne__(self, other):
        assert isinstance(other, SingleSkeleton)
        matched_count = 0
        for j1_id, j2_id in zip(self.joint_ids, other.joint_ids):
            if j1_id != -1 and j2_id != -1:
                matched_count += 1
        return matched_count == 0

    def assign(self, joint_type, joint_id, joint_x, joint_y, joint_score):
        assert self.device == 'cpu'
        self.joint_ids[joint_type] = joint_id
        self.joint_scores[joint_type] = joint_score
        if joint_x > 0 or joint_y > 0:
            self.joints[joint_type] = [joint_x, joint_y, 1]

    def avg_score(self):
        if self.device == 'cpu':
            return self.score
        else:
            return self.score / self.joint_count

    def add(self, joint_type, joint_id, limb, joint_x, joint_y, new_joint_score):
        self.joint_ids[joint_type] = joint_id
        self.joint_scores[joint_type] = limb.score
        self.joint_count += 1
        self.prev_limb_length = max(self.prev_limb_length, limb.length)
        self.score += new_joint_score + limb.score
        if joint_x > 0 or joint_y > 0:
            self.joints[joint_type][0] = joint_x
            self.joints[joint_type][1] = joint_y
            self.joints[joint_type][2] = 1

    def update(self, joint_type, joint_id, limb, old_joint_score, joint_x, joint_y, new_joint_score):
        self.score -= old_joint_score + self.joint_scores[joint_type]
        self.joint_ids[joint_type] = joint_id
        self.joint_scores[joint_type] = limb.score
        self.prev_limb_length = max(self.prev_limb_length, limb.length)
        self.score += new_joint_score + limb.score
        if joint_x > 0 or joint_y > 0:
            self.joints[joint_type][0] = joint_x
            self.joints[joint_type][1] = joint_y
            self.joints[joint_type][2] = 1
        else:
            self.joints[joint_type][0] = 0
            self.joints[joint_type][1] = 0
            self.joints[joint_type][2] = 0

    def cmp(self, other, cond):
        j1_min_score, j2_min_score = float(1e8), float(1e8)
        for idx, j1_id, j2_id in zip(range(self.num_keypoints), self.joint_ids, other.joint_ids):
            if j1_id != -1:
                j1_min_score = cond(j1_min_score, self.joint_scores[idx])
            if j2_id != -1:
                j2_min_score = cond(j2_min_score, other.joint_scores[idx])
        return cond(j1_min_score, j2_min_score)

    def merge(self, other, limb):
        for idx, j1_id, j2_id in zip(range(self.num_keypoints), self.joint_ids, other.joint_ids):
            assert not (j1_id != -1 and j2_id != -1)
            if j1_id == -1 and j2_id != -1:
                self.joint_ids[idx] = j2_id
                self.joint_scores[idx] = other.joint_scores[idx]

        self.prev_limb_length = max(limb.length, self.prev_limb_length)
        self.joint_count += other.joint_count
        self.score += other.score + limb.score
        self.joints = torch.max(self.joints, other.joints)