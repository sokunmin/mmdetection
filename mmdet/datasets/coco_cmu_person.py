import numpy as np
from enum import Enum
from pycocotools.coco import COCO

from . import CocoPersonDataset
from .registry import DATASETS


@DATASETS.register_module
class CocoCMUPersonDataset(CocoPersonDataset):

    CLASSES = ('person')
    KEYPOINTS = ['nose', 'neck', 'right shoulder', 'right elbow', 'right wrist', 'left shoulder',
                 'left elbow', 'left wrist', 'right hip', 'right knee', 'right ankle', 'left hip',
                 'left knee', 'left ankle', 'right eye', 'left eye', 'right ear', 'left ear']

    def load_annotations(self, ann_file):
        self.coco = COCO(ann_file)
        self.cat_ids = self.coco.getCatIds(catNms=['person'])
        self.cat2label = {
            cat_id: i + 1
            for i, cat_id in enumerate(self.cat_ids)
        }
        self.img_ids = self.coco.getImgIds(catIds=self.cat_ids)
        cats = self.coco.loadCats(self.cat_ids)[0]
        if 'skeleton' in cats:
            self.skeleton = np.array(cats['skeleton']) - 1
        else:
            print("The dataset json does not contain `skeleton` key.")
            self.skeleton = None
        self.num_keypoints = len(CocoCMUPersonDataset.KEYPOINTS)
        self.num_metric_keypoints = len(CocoPersonDataset.KEYPOINTS)
        self.joint_flip_pairs = np.array([[5, 2], [6, 3], [7, 4], [11, 8],
                                          [12, 9], [13, 10], [15, 14], [17, 16]])
        self.coco2cmu_order = [0, 17, 6, 8, 10, 5, 7, 9, 12, 14, 16, 11, 13, 15, 2, 1, 4, 3]
        self.joint_weights = np.ones((len(CocoCMUPersonDataset.KEYPOINTS), 1), dtype=np.float32)
        self.map_kp_v = {0: 2, 1: 0, 2: 1, 3: 3}
        img_infos = []
        for i in self.img_ids:
            info = self.coco.loadImgs([i])[0]
            info['filename'] = info['file_name']
            img_infos.append(info)
        return img_infos

    def get_ann_info(self, idx):
        img_id = self.img_infos[idx]['id']
        ann_ids = self.coco.getAnnIds(imgIds=[img_id], catIds=self.cat_ids, iscrowd=None)
        ann_info = self.coco.loadAnns(ann_ids)
        return self._parse_ann_info(self.img_infos[idx], ann_info)

    def _parse_ann_info(self, img_info, ann_info):
        """Parse bbox and mask annotation.

        Args:
            ann_info (list[dict]): Annotation info of an image.
            with_mask (bool): Whether to parse mask annotations.

        Returns:
            dict: A dict containing the following keys: bboxes, bboxes_ignore,
                labels, masks, seg_map. "masks" are raw annotations and not
                decoded into binary masks.
        """
        gt_bboxes = []
        gt_labels = []
        gt_bboxes_ignore = []
        gt_masks = []
        gt_keypoints = []
        gt_bboxes_centers = []
        gt_bboxes_scales = []
        gt_num_keypoints = []
        width, height = img_info['width'], img_info['height']
        gt_mask_all = np.zeros((height, width), dtype=np.uint8)
        gt_mask_miss = np.zeros((height, width), dtype=np.uint8)
        mask_crowd = None
        for i, ann in enumerate(ann_info):
            if ann.get('ignore', False) or ann['num_keypoints'] == 0:
                continue

            gt_num_keypoints.append(ann['num_keypoints'] + 1)

            x, y, w, h = ann['bbox']
            x1 = np.max((0, x))
            y1 = np.max((0, y))
            x2 = np.min((width - 1, x1 + np.max((0, w - 1))))
            y2 = np.min((height - 1, y1 + np.max((0, h - 1))))
            if ann['area'] <= 0 or w < 1 or h < 1 or x2 < x1 or y2 < y1:
                continue
            bbox = [x1, y1, x1 + w - 1, y1 + h - 1]
            bbox_centers = [x + w / 2, y + h / 2]
            bbox_scales = np.array(h) / height
            # > one instance can contain multiple pieces of masks
            mask = self.coco.annToMask(ann)
            # bbox2 = [x1, y1, x2, y2]
            if ann.get('iscrowd', False):
                mask_temp = np.bitwise_and(mask, gt_mask_all)
                mask_crowd = mask - mask_temp
                gt_bboxes_ignore.append(bbox)
            else:
                gt_masks.append(mask)
                gt_mask_all = np.bitwise_or(mask, gt_mask_all)
                if ann['num_keypoints'] <= 0:
                    gt_mask_miss = np.bitwise_or(mask, gt_mask_miss)
                # gt_masks.append(ann['segmentation'])
                gt_bboxes.append(bbox)
                gt_labels.append(self.cat2label[ann['category_id']])
                gt_bboxes_centers.append(bbox_centers)
                gt_bboxes_scales.append(bbox_scales)
                # keypoints
                gt_keypoint = np.zeros((self.num_keypoints, 3), dtype=np.float32)
                kp = np.append(np.array(ann['keypoints']), [0, 0, 3])  # `v=3`: never labeled
                gt_keypoint[:, 0] = kp[0::3]
                gt_keypoint[:, 1] = kp[1::3]
                # COCO - Each keypoint has a 0-indexed location x,y and a visibility flag v defined as
                # v=0: not labeled (in which case x=y=0),
                # v=1: labeled but not visible,
                # v=2: labeled and visible.
                # CMU
                #  0 - labeled but invisible
                #  1 - labeled and visible,
                #  2 - not labeled in this person
                #  3 - never labeled in this dataset
                # > `coco -> cmu`: 0:2, 1:0, 2:1, 3:3
                gt_keypoint[:, 2] = np.vectorize(self.map_kp_v.get)(kp[2::3])
                gt_keypoint = gt_keypoint[self.coco2cmu_order, :]
                # > add `neck` keypoint
                neck_idx = CocoCMUPersonDataset.KEYPOINTS.index('neck')
                rshoulder_idx = CocoCMUPersonDataset.KEYPOINTS.index('right shoulder')
                lshoulder_idx = CocoCMUPersonDataset.KEYPOINTS.index('left shoulder')
                shoulders_marked = (gt_keypoint[rshoulder_idx, 2] < 2) & (gt_keypoint[lshoulder_idx, 2] < 2)
                if shoulders_marked:
                    rshoulder_pt = gt_keypoint[rshoulder_idx]
                    lshoulder_pt = gt_keypoint[lshoulder_idx]
                    gt_keypoint[neck_idx, :2] = (rshoulder_pt[:2] + lshoulder_pt[:2]) / 2
                    gt_keypoint[neck_idx, 2] = np.minimum(rshoulder_pt[2], lshoulder_pt[2])
                gt_keypoints.append(gt_keypoint)

        if gt_bboxes:
            gt_bboxes = np.array(gt_bboxes, dtype=np.float32)
            gt_labels = np.array(gt_labels, dtype=np.int64)
        else:
            gt_bboxes = np.zeros((0, 4), dtype=np.float32)
            gt_labels = np.array([], dtype=np.int64)

        if gt_bboxes_ignore:
            gt_bboxes_ignore = np.array(gt_bboxes_ignore, dtype=np.float32)
        else:
            gt_bboxes_ignore = np.zeros((0, 4), dtype=np.float32)

        if mask_crowd is not None:
            gt_mask_miss = np.logical_not((np.bitwise_or(gt_mask_miss, mask_crowd)))
            gt_mask_all = np.bitwise_or(gt_mask_all, mask_crowd)
        else:
            gt_mask_miss = np.logical_not(gt_mask_miss)

        gt_mask_miss = gt_mask_miss.astype(np.uint8)
        gt_mask_all = gt_mask_all.astype(np.uint8)
        gt_mask_miss *= 255
        gt_mask_all *= 255
        gt_mask_ignore = np.concatenate(
            (gt_mask_all[None, :], gt_mask_miss[None, :]), axis=0)

        if gt_keypoints:
            gt_bboxes_centers = np.array(gt_bboxes_centers, dtype=np.float32)
            gt_bboxes_scales = np.array(gt_bboxes_scales, dtype=np.float32)
            gt_keypoints = np.array(gt_keypoints, dtype=np.float32)
        else:
            gt_bboxes_centers = np.zeros((0, 2), dtype=np.float32)
            gt_bboxes_scales = np.zeros((0, 1), dtype=np.float32)
            gt_keypoints = np.zeros((0, len(CocoCMUPersonDataset.KEYPOINTS), 3),
                                    dtype=np.float32)

        if gt_num_keypoints:
            gt_num_keypoints = np.array(gt_num_keypoints, dtype=np.int64)
        else:
            gt_num_keypoints = np.array([], dtype=np.int64)

        seg_map = img_info['filename'].replace('jpg', 'png')

        ann = dict(
            bboxes=gt_bboxes,
            labels=gt_labels,
            bboxes_ignore=gt_bboxes_ignore,
            bboxes_centers=gt_bboxes_centers,
            bboxes_scales=gt_bboxes_scales,
            masks=gt_masks,
            masks_ignore=gt_mask_ignore,
            seg_map=seg_map,
            keypoints=gt_keypoints,
            num_keypoints=gt_num_keypoints,
            flip_pairs=self.joint_flip_pairs,
        )

        return ann
