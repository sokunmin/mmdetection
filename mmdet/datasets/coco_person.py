import logging
import numpy as np
import mmcv
from enum import Enum
from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval

from . import CocoDataset
from .registry import DATASETS
from mmdet.utils import print_log


@DATASETS.register_module
class CocoPersonDataset(CocoDataset):

    CLASSES = ('person')
    KEYPOINTS = ['nose', 'left eye', 'right eye', 'left ear', 'right ear', 'left shoulder',
                 'right shoulder', 'left elbow', 'right elbow', 'left wrist', 'right wrist',
                 'left hip', 'right hip', 'left knee', 'right knee', 'left ankle', 'right ankle']

    def load_annotations(self, ann_file):
        self.coco = COCO(ann_file)
        self.cat_ids = self.coco.getCatIds(catNms=['person'])
        self.cat2label = {
            cat_id: i + 1
            for i, cat_id in enumerate(self.cat_ids)
        }
        self.img_ids = self.coco.getImgIds(catIds=self.cat_ids)
        cats = self.coco.loadCats(self.cat_ids)[0]
        if 'skeleton' in cats:
            self.skeleton = np.array(cats['skeleton']) - 1
        else:
            print("The dataset json does not contain `skeleton` key.")
            self.skeleton = None
        self.num_keypoints = len(CocoPersonDataset.KEYPOINTS)
        self.num_metric_keypoints = len(CocoPersonDataset.KEYPOINTS)
        self.joint_flip_pairs = np.array([[1, 2], [3, 4], [5, 6], [7, 8],
                                          [9, 10], [11, 12], [13, 14], [15, 16]])
        self.joint_weights = np.array([1., 1., 1., 1., 1., 1., 1., 1.2, 1.2,
                                       1.5, 1.5, 1., 1., 1.2, 1.2, 1.5, 1.5],
                                      dtype=np.float32).reshape((self.num_joints, 1))
        img_infos = []
        for i in self.img_ids:
            info = self.coco.loadImgs([i])[0]
            info['filename'] = info['file_name']
            img_infos.append(info)
        return img_infos

    def get_ann_info(self, idx):
        img_id = self.img_infos[idx]['id']
        ann_ids = self.coco.getAnnIds(imgIds=[img_id], catIds=self.cat_ids, iscrowd=None)
        ann_info = self.coco.loadAnns(ann_ids)
        return self._parse_ann_info(self.img_infos[idx], ann_info)

    def _parse_ann_info(self, img_info, ann_info):
        """Parse bbox and mask annotation.

        Args:
            ann_info (list[dict]): Annotation info of an image.
            with_mask (bool): Whether to parse mask annotations.

        Returns:
            dict: A dict containing the following keys: bboxes, bboxes_ignore,
                labels, masks, seg_map. "masks" are raw annotations and not
                decoded into binary masks.
        """
        gt_bboxes = []
        gt_labels = []
        gt_bboxes_ignore = []
        gt_masks_ann = []
        gt_keypoints = []
        width, height = img_info['width'], img_info['height']
        for i, ann in enumerate(ann_info):
            if ann.get('ignore', False):
                continue
            x, y, w, h = ann['bbox']
            x1 = np.max((0, x))
            y1 = np.max((0, y))
            x2 = np.min((width - 1, x1 + np.max((0, w - 1))))
            y2 = np.min((height - 1, y1 + np.max((0, h - 1))))
            if ann['area'] <= 0 or w < 1 or h < 1 or x2 < x1 or y2 < y1:
                continue
            bbox = [x1, y1, x1 + w - 1, y1 + h - 1]
            # bbox2 = [x1, y1, x2, y2]
            if ann.get('iscrowd', False):
                gt_bboxes_ignore.append(bbox)
            else:
                gt_bboxes.append(bbox)
                gt_labels.append(self.cat2label[ann['category_id']])
                gt_masks_ann.append(ann['segmentation'])
                # keypoints
                gt_keypoint = np.zeros((self.num_keypoints, 3), dtype=np.float32)
                kp = np.array(ann['keypoints'])
                gt_keypoint[:, 0] = kp[0::3]
                gt_keypoint[:, 1] = kp[1::3]
                gt_keypoint[:, 2] = (kp[2::3] > 0).astype(np.int32)
                gt_keypoints.append(gt_keypoint)

        if gt_bboxes:
            gt_bboxes = np.array(gt_bboxes, dtype=np.float32)
            gt_labels = np.array(gt_labels, dtype=np.int64)
        else:
            gt_bboxes = np.zeros((0, 4), dtype=np.float32)
            gt_labels = np.array([], dtype=np.int64)

        if gt_bboxes_ignore:
            gt_bboxes_ignore = np.array(gt_bboxes_ignore, dtype=np.float32)
        else:
            gt_bboxes_ignore = np.zeros((0, 4), dtype=np.float32)

        if gt_keypoints:
            gt_keypoints = np.array(gt_keypoints, dtype=np.float32)
        else:
            gt_keypoints = np.zeros((0, self.num_keypoints, 3), dtype=np.float32)

        seg_map = img_info['filename'].replace('jpg', 'png')

        ann = dict(
            bboxes=gt_bboxes,
            labels=gt_labels,
            bboxes_ignore=gt_bboxes_ignore,
            masks=gt_masks_ann,
            seg_map=seg_map,
            keypoints=gt_keypoints,
            flip_pairs=self.joint_flip_pairs,
        )

        return ann

    def results2json(self, results, outfile_prefix):
        """Dump the detection results to a json file.

        There are 3 types of results: proposals, bbox predictions, mask
        predictions, and they have different data types. This method will
        automatically recognize the type, and dump them to json files.

        Args:
            results (list[list | tuple | ndarray]): Testing results of the
                dataset.
            outfile_prefix (str): The filename prefix of the json files. If the
                prefix is "somepath/xxx", the json files will be named
                "somepath/xxx.bbox.json", "somepath/xxx.segm.json",
                "somepath/xxx.proposal.json".

        Returns:
            dict[str: str]: Possible keys are "bbox", "segm", "proposal", and
                values are corresponding filenames.
        """
        result_files = dict()
        if isinstance(results[0], list):
            json_results = self._pose2json(results)
            result_files['keypoints'] = '{}.{}.json'.format(outfile_prefix, 'keypoints')
            mmcv.dump(json_results, result_files['keypoints'])
        else:
            raise TypeError('invalid type of results')
        return result_files

    def evaluate(self,
                 results,
                 metric='bbox',
                 logger=None,
                 jsonfile_prefix=None,
                 classwise=False,
                 proposal_nums=(100, 300, 1000),
                 iou_thrs=np.arange(0.5, 0.96, 0.05)):
        """Evaluation in COCO protocol.

        Args:
            results (list): Testing results of the dataset.
            metric (str | list[str]): Metrics to be evaluated.
            logger (logging.Logger | str | None): Logger used for printing
                related information during evaluation. Default: None.
            jsonfile_prefix (str | None): The prefix of json files. It includes
                the file path and the prefix of filename, e.g., "a/b/prefix".
                If not specified, a temp file will be created. Default: None.
            classwise (bool): Whether to evaluating the AP for each class.
            proposal_nums (Sequence[int]): Proposal number used for evaluating
                recalls, such as recall@100, recall@1000.
                Default: (100, 300, 1000).
            iou_thrs (Sequence[float]): IoU threshold used for evaluating
                recalls. If set to a list, the average recall of all IoUs will
                also be computed. Default: 0.5.

        Returns:
            dict[str: float]
        """

        metrics = metric if isinstance(metric, list) else [metric]
        allowed_metrics = ['keypoints']
        for metric in metrics:
            if metric not in allowed_metrics:
                raise KeyError('metric {} is not supported'.format(metric))

        result_files, tmp_dir = self.format_results(results, jsonfile_prefix)

        eval_results = {}
        cocoGt = self.coco
        for metric in metrics:
            msg = 'Evaluating {}...'.format(metric)
            if logger is None:
                msg = '\n' + msg
            print_log(msg, logger=logger)

            if metric not in result_files:
                raise KeyError('{} is not in results'.format(metric))
            try:
                cocoDt = cocoGt.loadRes(result_files[metric])
            except IndexError:
                print_log(
                    'The testing results of the whole dataset is empty.',
                    logger=logger,
                    level=logging.ERROR)
                break

            iou_type = metric
            cocoEval = COCOeval(cocoGt, cocoDt, iou_type)
            cocoEval.params.imgIds = self.img_ids
            if metric == 'keypoints':
                cocoEval.evaluate()
                cocoEval.accumulate()
                cocoEval.summarize()
        if tmp_dir is not None:
            tmp_dir.cleanup()
        return eval_results