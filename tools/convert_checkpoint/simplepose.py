import torch
import os.path as osp
from collections import OrderedDict
from mmcv.runner import load_state_dict


def convert_conv_blocks(new_k):
    if 'convBlock' in new_k:
        if 'convBlock.0' in new_k:  # conv
            new_k = new_k.replace("convBlock.0", "conv1")
        elif 'convBlock.1' in new_k:  # bn
            new_k = new_k.replace("convBlock.1", "bn1")
        elif 'convBlock.3' in new_k:  # conv
            new_k = new_k.replace("convBlock.3", "conv2")
        elif 'convBlock.4' in new_k:  # bn
            new_k = new_k.replace("convBlock.4", "bn2")
        elif 'convBlock.6' in new_k:  # conv
            new_k = new_k.replace("convBlock.6", "conv3")
        elif 'convBlock.7' in new_k:  # bn
            new_k = new_k.replace("convBlock.7", "bn3")
    elif 'skipConv' in new_k:
        if 'skipConv.0' in new_k:
            new_k = new_k.replace("skipConv.0", "downsample.0")
        elif 'skipConv.1' in new_k:
            new_k = new_k.replace("skipConv.1", "downsample.1")
    return new_k


def load_checkpoint(model,
                    filename,
                    map_location=None,
                    strict=False,
                    logger=None,
                    show_model_arch=True,
                    print_value=True):
    """ Note that official pre-trained models use `BatchNorm` in backbone.
    """
    if not osp.isfile(filename):
        raise IOError('{} is not a checkpoint file'.format(filename))
    checkpoint = torch.load(filename, map_location=map_location)
    # get state_dict from checkpoint
    if isinstance(checkpoint, OrderedDict):
        state_dict = checkpoint
    elif isinstance(checkpoint, dict) and 'weights' in checkpoint:
        state_dict = checkpoint['weights']
    else:
        raise RuntimeError(
            'No state_dict found in checkpoint file {}'.format(filename))
    # strip prefix of state_dict
    if list(state_dict.keys())[0].startswith('posenet.'):
        state_dict = {}
        for k, v in checkpoint['weights'].items():
            new_k = k[8:]  # skip `posenet.`
            if 'pre.' in new_k:
                # > stem layers
                if 'pre.conv1.' in new_k or 'pre.bn1.' in new_k:
                    if 'conv1.' in new_k:
                        new_k = new_k.replace("conv1.", "conv1.conv.")
                    if 'bn1.' in new_k:
                        new_k = new_k.replace("bn1.", "conv1.bn.")
                # > res + skip
                if 'res1.' in new_k or 'res2.' in new_k:
                    new_k = convert_conv_blocks(new_k)

                    if 'res1.' in new_k:
                        new_k = new_k.replace("res1.", "res_layer1.0.")
                    elif 'res2.' in new_k:
                        new_k = new_k.replace("res2.", "res_layer2.0.")

                elif 'dilation.' in new_k:
                    new_k = new_k.replace("dilation.", "dilated_convs.")
                    new_k = new_k.replace(".conv.", ".conv.conv.")
                    new_k = new_k.replace(".bn.", ".conv.bn.")

                if 'pre.' in new_k:
                    new_k = new_k.replace("pre.", "backbone.")

            elif 'hourglass.' in new_k:
                if 'hourglass.0.' in new_k:
                    new_k = new_k.replace("hourglass.0.", "neck.hg_blocks.0.")
                elif 'hourglass.1.' in new_k:
                    new_k = new_k.replace("hourglass.1.", "neck.hg_blocks.1.")
                elif 'hourglass.2.' in new_k:
                    new_k = new_k.replace("hourglass.2.", "neck.hg_blocks.2.")
                elif 'hourglass.3.' in new_k:
                    new_k = new_k.replace("hourglass.3.", "neck.hg_blocks.3.")

                if '.0.0.' in new_k:
                    new_k = new_k.replace(".0.0.", ".0.0.0.")
                elif '.0.1.' in new_k:
                    new_k = new_k.replace(".0.1.", ".0.1.0.")
                elif '.0.2.' in new_k:
                    new_k = new_k.replace(".0.2.", ".0.2.0.")
                elif '.1.0.' in new_k:
                    new_k = new_k.replace(".1.0.", ".1.0.0.")
                elif '.1.1.' in new_k:
                    new_k = new_k.replace(".1.1.", ".1.1.0.")
                elif '.1.2.' in new_k:
                    new_k = new_k.replace(".1.2.", ".1.2.0.")
                elif '.2.0.' in new_k:
                    new_k = new_k.replace(".2.0.", ".2.0.0.")
                elif '.2.1.' in new_k:
                    new_k = new_k.replace(".2.1.", ".2.1.0.")
                elif '.2.2.' in new_k:
                    new_k = new_k.replace(".2.2.", ".2.2.0.")
                elif '.3.0.' in new_k:
                    new_k = new_k.replace(".3.0.", ".3.0.0.")
                elif '.3.1.' in new_k:
                    new_k = new_k.replace(".3.1.", ".3.1.0.")
                elif '.3.2.' in new_k:
                    new_k = new_k.replace(".3.2.", ".3.2.0.")
                elif '.3.4.' in new_k:
                    new_k = new_k.replace(".3.4.", ".3.4.0.")

                new_k = convert_conv_blocks(new_k)

            elif 'outs.' in new_k:
                new_k = new_k.replace("outs.", "neck.out_convs.")
                new_k = new_k.replace(".conv.", ".")

            elif 'merge_features.' in new_k:
                new_k = new_k.replace("merge_features.", "neck.cache_feat.")
                new_k = new_k.replace(".conv.", ".")

            elif 'merge_preds.' in new_k:
                new_k = new_k.replace("merge_preds.", "neck.cache_out.")
                new_k = new_k.replace(".conv.", ".")

            elif 'features.' in new_k:
                new_k = new_k.replace("features.", "neck.se_convs.")
                new_k = new_k.replace("before_regress.", "")

            if print_value:
                print('> key =', k, ' \t-> ', new_k)

            state_dict[new_k] = v

    if show_model_arch:
        print('> model = ', model)
    # load state_dict
    if hasattr(model, 'module'):
        load_state_dict(model.module, state_dict, strict, logger)
    else:
        load_state_dict(model, state_dict, strict, logger)
