import torch
import os.path as osp
from collections import OrderedDict
from mmcv.runner import load_state_dict


def load_checkpoint(model,
                    filename,
                    map_location=None,
                    strict=False,
                    logger=None,
                    show_model_arch=False,
                    print_value=False):
    """ Note that official pre-trained models use `GroupNorm` in backbone.
    """
    if not osp.isfile(filename):
        raise IOError('{} is not a checkpoint file'.format(filename))
    checkpoint = torch.load(filename, map_location=map_location)
    # get state_dict from checkpoint
    if isinstance(checkpoint, OrderedDict):
        state_dict = checkpoint
    elif isinstance(checkpoint, dict) and 'model' in checkpoint:
        state_dict = checkpoint['model']
    else:
        raise RuntimeError(
            'No state_dict found in checkpoint file {}'.format(filename))
    # strip prefix of state_dict
    if list(state_dict.keys())[0].startswith('module.'):
        state_dict = {}
        for k, v in checkpoint['model'].items():
            new_k = k[7:]
            if 'backbone' in new_k:
                if 'body.' in new_k:
                    new_k = new_k.replace("body.", "")
                if 'stem.' in new_k:
                    new_k = new_k.replace("stem.", "")
                if 'fpn' in new_k:
                    if 'fpn_layer' in new_k:
                        new_k = new_k.replace("backbone.fpn.fpn_layer", "neck.fpn_convs.")
                        if '2.' in new_k:
                            new_k = new_k.replace("2.", "0.conv.")
                        elif '3.' in new_k:
                            new_k = new_k.replace("3.", "1.conv.")
                        elif '4.' in new_k:
                            new_k = new_k.replace("4.", "2.conv.")
                    elif 'top_blocks' in new_k:
                        new_k = new_k.replace("backbone.fpn.top_blocks.", "neck.fpn_convs.")
                        if 'p6.' in new_k:
                            new_k = new_k.replace("p6.", "3.conv.")
                        elif 'p7.' in new_k:
                            new_k = new_k.replace("p7.", "4.conv.")
                    elif 'fpn_inner' in new_k:
                        new_k = new_k.replace("backbone.fpn.fpn_inner", "neck.lateral_convs.")
                        if '2.' in new_k:
                            new_k = new_k.replace("2.", "0.conv.")
                        elif '3.' in new_k:
                            new_k = new_k.replace("3.", "1.conv.")
                        elif '4.' in new_k:
                            new_k = new_k.replace("4.", "2.conv.")
            elif 'rpn.head.' in new_k:
                new_k = new_k.replace("rpn.head.", "")
                if 'tower' in new_k:
                    if 'cls_tower' in new_k:
                        new_k = new_k.replace("cls_tower", "bbox_head.cls_convs")
                    elif 'bbox_tower' in new_k:
                        new_k = new_k.replace("bbox_tower", "bbox_head.reg_convs")
                    elif 'mask_tower' in new_k:
                        new_k = new_k.replace("mask_tower", "mask_head.mask_convs")
                if 'scale' in new_k:
                    if 'scales' in new_k:
                        new_k = new_k.replace("scales.", "bbox_head.scales.")
                else:
                    if '1.' in new_k:
                        new_k = new_k.replace("1.", "0.gn.")
                    elif '4.' in new_k:
                        new_k = new_k.replace("4.", "1.gn.")
                    elif '7.' in new_k:
                        new_k = new_k.replace("7.", "2.gn.")
                    elif '10.' in new_k:
                        new_k = new_k.replace("10.", "3.gn.")
                    elif '0.' in new_k:
                        new_k = new_k.replace("0.", "0.conv.")
                    elif '3.' in new_k:
                        new_k = new_k.replace("3.", "1.conv.")
                    elif '6.' in new_k:
                        new_k = new_k.replace("6.", "2.conv.")
                    elif '9.' in new_k:
                        new_k = new_k.replace("9.", "3.conv.")

                    if 'cls_logits' in new_k:
                        new_k = new_k.replace("cls_logits", "bbox_head.fcos_cls")
                    if 'bbox_pred' in new_k:
                        new_k = new_k.replace("bbox_pred", "bbox_head.fcos_reg")
                    if 'centerness' in new_k:
                        new_k = new_k.replace("centerness", "bbox_head.fcos_centerness")
                    if 'proposal_embed_pred' in new_k:
                        new_k = new_k.replace("proposal_embed_pred", "mask_head.proposal_embed")
                    if 'proposal_margin_pred' in new_k:
                        new_k = new_k.replace("proposal_margin_pred", "mask_head.proposal_margin")
                    if 'pixel_embed_pred' in new_k:
                        new_k = new_k.replace("pixel_embed_pred", "mask_head.pixel_embed")

            if print_value:
                print('> key = ', k, ' -> ', new_k, ' = ', v)
            state_dict[new_k] = v
    if show_model_arch:
        print('> model = ', model)
    # load state_dict
    if hasattr(model, 'module'):
        load_state_dict(model.module, state_dict, strict, logger)
    else:
        load_state_dict(model, state_dict, strict, logger)
