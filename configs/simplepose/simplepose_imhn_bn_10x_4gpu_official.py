num_keypoints = 18
# model settings
model = dict(
    type='SimplePose',
    pretrained='weights/simplepose_imhn_epoch52.pth',
    backbone=dict(
        type='PRM',
        feat_channels=128,
        dilations=(3, 3, 4, 4, 5, 5),
        norm_cfg=dict(type='BN', requires_grad=True),
        act_cfg=dict(type='LeakyReLU',
                     negative_slope=0.01,
                     inplace=True)),
    neck=dict(
        type='IMHFPN',
        in_channels=256,
        out_channels=50,
        stride=2,
        num_stages=4,
        num_outs=5,
        depth=4,
        norm_cfg=dict(type='BN', requires_grad=True),
        act_cfg=dict(
            type='LeakyReLU',
            negative_slope=0.01,
            inplace=True)),
    keypoint_head=dict(
        type='SimplePoseHead',
        num_classes=50,  # paf:30, hp: 20 (18+2)
        in_channels=50,
        stacked_convs=4,
        feat_channels=256,
        target_type='gaussian',
        strides=[4, 8, 16, 32, 64],
        joint_cfg=dict(
            num_joints=num_keypoints,
            num_bg=2,
            sigma=2,
            thr=0.1,
        ),
        bodypart_cfg=dict(
            num_bodyparts=30,
            intermd_points=20,
            min_intermd_factor=0.8,
            thr=0.1,
            weights=[0.5, 0.25, 0.25]
        ),
        skeleton_cfg=dict(
            min_length_ratio=16,
            min_score_ratio=0.7,
            delete_shared_joints=False
        ),
        gen_cfg=dict(
            joint_sigma=9,
            bodypart_sigma=7,
            bodypart_min_sigma=4,
            joint_thr=0.015,
            bodypart_thr=0.015,
            joint_weight=3,
            bg_weight=0.1
        ),
        norm_cfg=dict(type='BN', requires_grad=True),
        act_cfg=dict(
            type='LeakyReLU',
            negative_slope=0.01,
            inplace=True),
        loss_joint=dict(
            type='FocalL2Loss',
            use_sigmoid=True,
            gamma=1.0,
            alpha=0.1,
            beta=0.02,
            target_thr=0.01,
            loss_weight=1.0,
            scale_weights=[0.1, 0.2, 0.4, 1.6, 6.4])))
# training and testing settings
train_cfg = dict(
    assigner=dict(
        type='MaxIoUAssigner',
        pos_iou_thr=0.5,
        neg_iou_thr=0.4,
        min_pos_iou=0,
        ignore_iof_thr=-1),
    allowed_border=-1,
    pos_weight=-1,
    debug=False)
test_cfg = dict(
    nms_pre=1000,
    min_bbox_size=0,
    score_thr=0.05,
    nms=dict(type='nms', iou_thr=0.5),
    max_per_img=100)
# dataset settings
dataset_type = 'CocoCMUPersonDataset'
data_root = 'data/coco/'
# img_norm_cfg = dict(
#     mean=[102.9801, 115.9465, 122.7717], std=[1.0, 1.0, 1.0], to_rgb=False)
img_norm_cfg = dict(
    mean=[0., 0., 0.], std=[255., 255., 255.], to_rgb=False)
train_pipeline = [
    dict(type='LoadImageFromFile'),
    dict(type='LoadAnnotations', with_bbox=True, with_mask=True,
         with_keypoint=True, poly2mask=False),
    dict(type='DistortColor', tint_ratio=0.2, jitter_offset=(10, 20, 20),
         jitter_range=(20, 80, 60), jitter_max=(179, 255, 255)),
    dict(type='AffineTransform',
         img_scale=(512, 512),
         ratio_mode='range',
         flip_ratio=0.5,
         flip_direction='horizontal',
         shift_pixels=50,
         scale_ratio=0.8,
         scale_range=(0.7, 1.3),
         min_occupation_ratio=0.6,
         rotate_angle=40,
         img_pad_value=(124, 127, 127),
         mask_pad_value=[(0, 255), (0,)],
         mask2bbox=True),
    # dict(type='Resize', img_scale=(512, 512), keep_ratio=True),
    # dict(type='RandomFlip', flip_ratio=0.5),
    dict(type='Normalize', **img_norm_cfg),
    dict(type='Pad', size_divisor=32),
    dict(type='DefaultFormatBundle'),
    dict(type='Collect',
         keys=['img', 'gt_bboxes', 'gt_labels', 'gt_masks', 'gt_masks_ignore', 'gt_keypoints']),
]
test_pipeline = [
    dict(type='LoadImageFromFile'),
    dict(
        type='MultiScaleFlipAug',
        img_scale=1.0,  # TOCHECK: official cfg -> mmdet cfg
        flip=False,
        transforms=[
            dict(type='Resize', keep_ratio=True),
            # dict(type='RandomFlip'),
            dict(type='Pad', size_divisor=64, pad_val=128),  # TOCHECK:  `pad_value`: 128 -> 0, `divisor`: 32 -> 64
            dict(type='Normalize', **img_norm_cfg),  # TOCHECK: pad <-> normalize
            dict(type='ImageToTensor', keys=['img']),
            dict(type='Collect', keys=['img']),
        ])
]
data = dict(
    imgs_per_gpu=2,
    workers_per_gpu=0,
    train=dict(
        type=dataset_type,
        ann_file=data_root + 'annotations/person_keypoints_train2017.json',
        img_prefix=data_root + 'images/train2017/',
        pipeline=train_pipeline),
    val=dict(
        type=dataset_type,
        ann_file=data_root + 'annotations/person_keypoints_val2017.json',
        img_prefix=data_root + 'images/val2017/',
        pipeline=test_pipeline),
    test=dict(
        type=dataset_type,
        ann_file=data_root + 'annotations/person_keypoints_val2017.json',
        img_prefix=data_root + 'images/val2017/',
        pipeline=test_pipeline))
evaluation = dict(interval=1, metric='keypoints')
# optimizer
optimizer = dict(
    type='SGD',
    lr=2.5e-5,
    momentum=0.9,
    weight_decay=0.0001,
    paramwise_options=dict(bias_lr_mult=2., bias_decay_mult=0.))
optimizer_config = dict(grad_clip=None)
# learning policy
lr_config = dict(
    policy='step',
    warmup='linear',
    warmup_iters=500,
    warmup_ratio=1.0 / 3,
    step=[8, 11])
checkpoint_config = dict(interval=1)
# yapf:disable
log_config = dict(
    interval=50,
    hooks=[
        dict(type='TextLoggerHook'),
        dict(type='TensorboardLoggerHook')
    ])
# yapf:enable
# runtime settings
total_epochs = 12
dist_params = dict(backend='nccl')
log_level = 'INFO'
work_dir = './work_dirs/simplepose_imhn_bn_10x_4gpu'
load_from = None
resume_from = None
workflow = [('train', 1)]
