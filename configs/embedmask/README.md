# EmbedMask: Embedding Coupling for One-stage Instance Segmentation

## Introduction

```
@misc{ying2019embedmask,
    title={EmbedMask: Embedding Coupling for One-stage Instance Segmentation},
    author={Hui Ying and Zhaojin Huang and Shu Liu and Tianjia Shao and Kun Zhou},
    year={2019},
    eprint={1912.01954},
    archivePrefix={arXiv},
    primaryClass={cs.CV}
}
```

## Results and Models

| Backbone  | Style   | GN  | MS train | Lr schd | Mem (GB) | Train time (s/iter) | Inf time (fps) | box AP | Download |
|:---------:|:-------:|:-------:|:-------:|:-------:|:--------:|:-------------------:|:--------------:|:------:|:--------:|
| R-50      | caffe   | Y       | N       | 1x      | -        | -               | -           | -   | - |
| R-101     | caffe   | Y       | N       | 1x      | -        | -               | -           | -   | - |
| X-101     | caffe   | Y       | N       | 1x      | -        | -               | -           | -   | - |



**Notes:**
- To be consistent with the author's implementation, we use 4 GPUs with 4 images/GPU for R-50 and R-101 models, and 8 GPUs with 2 image/GPU for X-101 models.
- The X-101 backbone is X-101-64x4d.
